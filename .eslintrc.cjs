module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    // '@typhonjs-config/eslint-config/esm/2022/browser',
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    '@typhonjs-fvtt/eslint-config-foundry.js/0.8.0',
    'prettier',
    'plugin:jest-dom/recommended',
  ],
  parserOptions: {
    project: './tsconfig.eslint.json',
    sourceType: 'module',
    ecmaVersion: 2020,
  },
  plugins: ['svelte3', 'jest-dom'],
  parser: '@typescript-eslint/parser',
  overrides: [
    {
      files: ['*.svelte'],
      processor: 'svelte3/svelte3',
      plugins: ['svelte3', 'jest-dom'],
    },
  ],
  rules: {
    '@typescript-eslint/no-explicit-any': 'off',
    // 'no-console': 'off',
  },
  settings: {
    'svelte3/typescript': () => require('typescript'),
  },
}
