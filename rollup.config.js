import fs from 'fs'
import path from 'path'
import * as url from 'url'
import glob from 'glob'

import dotenv from 'dotenv-safe' // Provides flexible definition of environment variables.

// The following plugins are for the main source bundle.

import copy from 'rollup-plugin-copy' // Copies files
import json from '@rollup/plugin-json' // Allows import of JSON; used in dialog Handlebars content.
import postcss from 'rollup-plugin-postcss' // Process Sass / CSS w/ PostCSS
// import replace from '@rollup/plugin-replace' // Replaces text in processed source files.
import { string } from 'rollup-plugin-string' // Allows loading strings as ES6 modules.

import typescript from '@rollup/plugin-typescript'
import svelte from 'rollup-plugin-svelte'
import sveltePreprocess from 'svelte-preprocess'
import cleaner from 'rollup-plugin-cleaner'

// The following plugins are for the 2nd & 3rd external bundles pulling in modules from NPM.
import { nodeResolve } from '@rollup/plugin-node-resolve' // This resolves NPM modules from node_modules.
// import { type } from 'os'

// Terser is used as an output plugin in both bundles to conditionally minify / mangle the output bundles depending
// on which NPM script & .env file is referenced.

// import { terser } from '@el3um4s/rollup-plugin-terser' // Terser is used for minification / mangling

import eslint from '@rollup/plugin-eslint'

// Import config files for Terser and Postcss; refer to respective documentation for more information.
// We are using `require` here in order to be compliant w/ `fvttdev` for testing purposes.
// const terserConfig = require('./terser.config')
import terserConfig from './terser.config.js'
import terser from '@rollup/plugin-terser'
import postcssConfig from './postcss.config.cjs'

export default () => {
  // Load the .env file specified in the command line target into process.env using `dotenv-safe`
  // This is a very convenient and cross platform way to handle environment variables. Please note that the .env
  // files are committed to this repo, but in your module you should uncomment the `**.env` outputDirectoryective in `.gitignore`
  // so that .env files are not committed into your repo. The difference between `dotenv-safe` and `dotenv` is that
  // the former provides a template providing a sanity check for imported .env files to make sure all required
  // parameters are present. This template is located in `./env/.env.example` and can be checked into your repo.

  // There are two environment variables loaded from .env files.
  // process.env.FVTTDEV_DEPLOY_PATH is the full path to the destination of your module / bundled code.
  // process.env.FVTTDEV_COMPRESS specifies if the bundled code should be minified.
  // process.env.FVTTDEV_SOURCEMAPS specifies if the source maps should be generated.

  // process.env.TARGET is defined in package.json NPM scripts using the `cross-env` NPM module passing it into
  // running this script. It defines which .env file to use below.

  const __dirname = url.fileURLToPath(new URL('.', import.meta.url))
  dotenv.config({
    example: `${__dirname}/env/.env.example`,
    path: `${__dirname}/env/${process.env.TARGET}.env`,
  })

  // Sanity check to make sure parent directory of FVTTDEV_DEPLOY_PATH exists.
  if (!fs.existsSync(path.dirname(process.env.FVTTDEV_DEPLOY_PATH))) {
    throw Error(
      `FVTTDEV_DEPLOY_PATH does not exist: ${process.env.FVTTDEV_DEPLOY_PATH}`
    )
  }

  // Reverse relative path from the deploy path to local outputDirectoryectory; used to replace source maps path.
  const relativePath = path.relative(process.env.FVTTDEV_DEPLOY_PATH, '.')

  // Defines potential output plugins to use conditionally if the .env file indicates the bundles should be
  // minified / mangled.
  const outputPlugins = []
  if (process.env.FVTTDEV_COMPRESS === 'true') {
    outputPlugins.push(terser(terserConfig))
  }

  // Defines whether source maps are generated / loaded from the .env file.
  const sourcemap = process.env.FVTTDEV_SOURCEMAPS === 'true'

  // Manually set `sourceMap` for PostCSS configuration.
  postcssConfig.sourceMap = sourcemap // Potentially generate sourcemaps

  // Shortcuts
  const outputDirectory = process.env.FVTTDEV_DEPLOY_PATH

  const copyTargets = [
    { src: `./src/system.json`, dest: outputDirectory }, // Copies module.json to destination.
    { src: `./src/template.json`, dest: outputDirectory }, // Copies module.json to destination.
    { src: `./src/lang`, dest: outputDirectory },
    { src: `./src/packs`, dest: outputDirectory },
    { src: `./src/templates`, dest: outputDirectory },
  ]

  if (sourcemap) {
    copyTargets.push({ src: `./src`, dest: outputDirectory })
  }

  const inputPlugins = [
    watchGlobs(['./src/templates/**/*.html']),
    postcss(postcssConfig), // Engages PostCSS for Sass / CSS processing
    // eslint({
    //   exclude: '**/node_modules/**',
    // }),
    typescript(),
    nodeResolve({ main: false }),
    svelte({
      preprocess: sveltePreprocess(),
    }),
    json(), // Allows import of JSON; used in dialog Handlebars content.
    string({ include: ['**/*.css', '**/*.html'] }), // Allows loading strings as ES6 modules; HTML and CSS.
    copy({
      targets: copyTargets,
    }),
  ]

  const isCleanBuild = process.env.FVTTDEV_CLEAN === 'true'
  if (isCleanBuild) {
    console.log('Clean build.')
    inputPlugins.push(
      cleaner({
        targets: [outputDirectory],
      })
    )
  }

  console.log(`Bundling target: ${process.env.TARGET}`)

  return [
    {
      input: `./src/dsa-4.1.ts`,
      output: {
        dir: outputDirectory,
        format: 'es',
        preferConst: true,
        minifyInternalExports: false,
        exports: 'auto',
        chunkFileNames: '[name].js',
        plugins: outputPlugins,
        sourcemap,
        sourcemapPathTransform: (sourcePath) =>
          sourcePath.replace(relativePath, `.`),
      },
      plugins: inputPlugins,
      manualChunks(id) {
        if (id.includes('node_modules')) {
          return 'vendor'
        }
      },
    },
  ]
}

// Watch any arbitrary project files for changes, such as static assets
// (source: <https://github.com/rollup/rollup/issues/3414#issuecomment-751699335>)
function watchGlobs(globs) {
  return {
    name: 'rollup-watch-globs',
    buildStart() {
      const items = Array.isArray(globs) ? globs : [globs];
      console.log('Watch Globs build start: ', items)
      items.forEach((item) => {
        glob.sync(item).forEach((filename) => {
          this.addWatchFile(filename)
        })
      })
    },
  }
}
