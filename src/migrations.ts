import { getGame } from './module/utils.js'

export async function migrateSystem(): Promise<void> {
  const game = getGame()
  migrateSystemName()

  if (!game.user?.isGM) return
  await migrateSkillToSpecialAbility()
}

async function migrateSkillToSpecialAbility() {
  const game = getGame()
  if (game !== undefined) {
    if (game.items) {
      await migrateSkillToSpecialAbilityInContext(game.items)
    }
    for (const actor of game.actors ?? []) {
      await migrateSkillToSpecialAbilityInContext(actor.items, actor)
    }
    for (const scene of game.scenes || []) {
      for (const token of scene.tokens || []) {
        if (token.actor !== null) {
          await migrateSkillToSpecialAbilityInContext(
            token.actor.items,
            token.actor
          )
        }
      }
    }
  }
}

async function migrateSkillToSpecialAbilityInContext(items: any, parent?: any) {
  const createData =
    items
      ?.filter((item) => item.system.type === 'skill')
      .map((item) => {
        const newData = duplicate(item.system)
        delete newData._id
        newData.type = 'specialAbility'
        return newData as any
      }) || []
  const deleteIDs =
    items
      ?.filter((item) => item.system.type === 'skill')
      .map((item) => item.id || '') || []
  if (deleteIDs.length > 0 || createData.length > 0) {
    if (parent === undefined) {
      await Item.createDocuments(createData)
      await Item.deleteDocuments(deleteIDs)
    } else {
      parent.createEmbeddedDocuments('Item', createData)
      parent.deleteEmbeddedDocuments('Item', deleteIDs)
    }
  }
}

type FlagConfig = {
  key: string
  owners: (Combatant | Actor)[]
}

async function migrateSystemName() {
  const game = getGame()
  const actors = game.actors?.contents || []
  for (const scene of game.scenes || []) {
    for (const token of scene.tokens || []) {
      if (token.actor !== null) {
        actors.push(token.actor)
      }
    }
  }

  const combatants =
    game.combats?.map((combat) => combat.combatants.contents || []).flat() || []

  const flags: FlagConfig[] = [
    { key: 'lastInitiative', owners: combatants },
    { key: 'last-offensive-maneuver', owners: actors },
    { key: 'last-defensive-maneuver', owners: actors },
  ]
  game.modules.set('dsa-4.1', { id: 'dsa-4.1', active: true })
  const orgGetFlagScopes = CONFIG.DatabaseBackend.getFlagScopes
  CONFIG.DatabaseBackend.getFlagScopes = () => {
    const scopes = ['core', 'world', game.system.id]
    for (const module of game.modules) {
      if (module.active) scopes.push(module.id)
    }
    return scopes
  }
  for (const flag of flags) {
    for (const owner of flag.owners) {
      const value = owner.getFlag('dsa-4.1', flag.key)

      if (value !== null && value !== undefined) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        //@ts-ignore
        await owner.setFlag('dsa-41', flag.key, value)
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        //@ts-ignore
        await owner.setFlag('dsa-4.1', flag.key, null)
      }
    }
  }
  game.modules.delete('dsa-4.1')
  CONFIG.DatabaseBackend.getFlagScopes = orgGetFlagScopes

  const settingKeys = Array.from(game.settings.settings.keys())
    .filter((key) => key.startsWith('dsa-4.1'))
    .map((key) => key.replace('dsa-4.1', ''))
  for (const key of settingKeys) {
    const value = game.settings.get('dsa-4.1', key)

    if (value !== null && value !== undefined) {
      await game.settings.set('dsa-41', key, value)
      await game.settings.set('dsa-4.1', key, null)
    }
  }
}
