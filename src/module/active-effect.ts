import { Attribute, CombatAttribute } from './enums.js'
import { labeledEnumValues } from './i18n.js'
import { getLocalizer } from './utils.js'

const localize = getLocalizer()

interface DsaActiveEffectConfigData extends DocumentSheetOptions {
  changeableAttributes: Record<string, string>
  effect: ActiveEffect
}

// eslint-disable-next-line no-undef
export class DsaActiveEffectConfig extends ActiveEffectConfig<DsaActiveEffectConfigData> {
  get template(): string {
    return `systems/dsa-41/templates/active-effect.html`
  }

  async getData(
    options: Application.RenderOptions | undefined
  ): Promise<DsaActiveEffectConfigData> {
    const data = (await super.getData(options)) as DsaActiveEffectConfigData

    const attributes = labeledEnumValues(Attribute, false)
    const combatAttributes = labeledEnumValues(CombatAttribute, false)

    data.changeableAttributes = attributes.reduce((prev, labeledValue) => {
      const key = `system.base.basicAttributes.${labeledValue.value}.value`
      return { ...prev, [key]: labeledValue.label }
    }, {})

    data.changeableAttributes = combatAttributes.reduce(
      (prev, labeledValue) => {
        const key = `system.base.combatAttributes.active.${labeledValue.value}.value`
        return { ...prev, [key]: labeledValue.label }
      },
      data.changeableAttributes
    )

    data.changeableAttributes[
      'system.base.combatAttributes.passive.magicResistance.value'
    ] = localize('magicResistance')

    data.changeableAttributes['system.base.movement.speed.value'] =
      localize('speed')

    data.changeableAttributes['system.additionalItems'] =
      localize('additionalItem')

    data.effect.changes = data.effect.changes.map((change) => {
      if (change.key === 'system.additionalItems') {
        change.value =
          typeof change.value === 'string'
            ? { ...JSON.parse(change.value), json: change.value }
            : change.value
      }
      return change
    })

    return data
  }

  async _onDrop(event) {
    // Try to extract the data
    let data
    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'))
    } catch (err) {
      return false
    }

    if (data.type === 'Item') {
      const item = await Item.fromDropData(data)
      if (item !== undefined) {
        const idx = this.document.changes.length
        return this.submit({
          preventClose: true,
          updateData: {
            [`changes.${idx}`]: {
              key: 'system.additionalItems',
              mode: CONST.ACTIVE_EFFECT_MODES.ADD,
              value: JSON.stringify({
                name: item.name,
                uuid: data.uuid,
              }),
            },
          },
        })
      }
    }
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      dragDrop: [{ dragSelector: null, dropSelector: null }],
    })
  }

  _getSubmitData(updateData = {}) {
    const data = super._getSubmitData(updateData)
    data.changes = data.changes.map((change) => {
      if (change.key === 'system.additionalItems' && change.extraValue) {
        change.value = JSON.stringify({
          ...JSON.parse(change.value),
          value: change.extraValue,
        })
      }
      return change
    })
    return data
  }
}
