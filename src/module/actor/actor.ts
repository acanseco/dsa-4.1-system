import { DsaRoll, critical1d20 } from '../dsa-roll.js'
import { ComputeRangedAttack } from '../ruleset/rules/derived-combat-attributes.js'
import { getGame } from '../utils.js'
import {
  CharacterDataAccessor,
  CombatState,
  MeleeCombatOptions,
} from '../model/character.js'
import { Rollable, NamedAttribute } from '../model/properties.js'
import { Attribute } from '../character/attribute.js'
import { AttributeName, CharacterData } from '../model/character-data.js'
import {
  BaseDataAccessor,
  DataAccessor,
  isProperty,
  ItemType,
  PropertyItemType,
  TalentItemType,
  TestableSkillItemType,
  WeaponItemType,
} from '../model/item-data.js'
import { DsaItem } from '../item/item.js'
import { GenericWeapon } from '../character/weapon.js'
import { Character } from '../character/character.js'
import { GenericCombatTalent, TestableSkill } from '../character/skill.js'
import {
  ComputeBaseAttack,
  ComputeBaseInitiative,
  ComputeBaseParry,
  ComputeBaseRangedAttack,
  ComputeMagicResistance,
} from '../ruleset/rules/derived-attributes.js'

declare global {
  interface DocumentClassConfig {
    Actor: typeof DsaActor
  }
}

type AdditionalItemData = {
  uuid?: string
  name: string
  value?: number
}

interface ExtendedCharacterData extends CharacterData {
  additionalItems: AdditionalItemData[]
}

interface DsaActorDataSource {
  type: 'character'
  system: CharacterData
  data: CharacterData
}
interface DsaActorDataProperties extends DsaActorDataSource {
  system: ExtendedCharacterData
  data: ExtendedCharacterData
}

type ActorDataSource = DsaActorDataSource
type ActorDataProperties = DsaActorDataProperties

declare global {
  interface SourceConfig {
    Actor: ActorDataSource
  }

  interface DataConfig {
    Actor: ActorDataProperties
  }
}

function hasType<Type extends ItemType>(type: Type) {
  return (item: BaseDataAccessor<ItemType>): item is BaseDataAccessor<Type> =>
    item.type === type
}

export class DsaActor extends Actor implements CharacterDataAccessor {
  public declare system: ExtendedCharacterData
  public tempItemPromise: Promise<any>

  attribute(name: AttributeName): Rollable<NamedAttribute> {
    return new Attribute(this.system, name)
  }
  get baseAttack(): number {
    return this.system.base.combatAttributes.active.baseAttack.value
  }
  get baseParry(): number {
    return this.system.base.combatAttributes.active.baseParry.value
  }
  get baseRangedAttack(): number {
    return this.system.base.combatAttributes.active.baseRangedAttack.value
  }
  get baseInitiative(): number {
    return this.system.base.combatAttributes.active.baseInitiative.value
  }
  get dodge(): number {
    return this.system.base.combatAttributes.active.dodge.value
  }

  private property(sid: string): DataAccessor<PropertyItemType> | undefined {
    return (this.items.contents as DataAccessor[]).find(
      (item: DataAccessor): item is DataAccessor<PropertyItemType> =>
        isProperty(item?.system) && item?.system?.sid === sid
    )
  }

  talent(sid: string): DataAccessor<TalentItemType> | undefined {
    return this.property(sid) as DataAccessor<TalentItemType> | undefined
  }
  spell(sid: string): DataAccessor<'spell'> | undefined {
    return this.property(sid) as DataAccessor<'spell'> | undefined
  }

  liturgy(sid: string): DataAccessor<'liturgy'> | undefined {
    return this.property(sid) as DataAccessor<'liturgy'> | undefined
  }

  get properties(): DataAccessor<PropertyItemType>[] {
    return (this.items.contents as DataAccessor[]).filter(
      (item: DataAccessor): item is DataAccessor<PropertyItemType> =>
        isProperty(item?.system)
    )
  }
  get specialAbilities(): DataAccessor<'specialAbility'>[] {
    return (this.items.contents as DataAccessor[]).filter(
      hasType('specialAbility')
    )
  }
  get advantages(): DataAccessor<'advantage'>[] {
    return (this.items.contents as DataAccessor[]).filter(hasType('advantage'))
  }
  get disadvantages(): DataAccessor<'disadvantage'>[] {
    return (this.items.contents as DataAccessor[]).filter(
      hasType('disadvantage')
    )
  }

  advantage(sid: string): DataAccessor<'advantage'> | undefined {
    return this.property(sid) as DataAccessor<'advantage'>
  }
  disadvantage(sid: string): DataAccessor<'disadvantage'> | undefined {
    return this.property(sid) as DataAccessor<'disadvantage'>
  }
  specialAbility(sid: string): DataAccessor<'specialAbility'> | undefined {
    return this.property(sid) as DataAccessor<'specialAbility'>
  }
  get armorItems(): DataAccessor<'armor'>[] {
    return (this.items.contents as DataAccessor[]).filter(
      (item: DataAccessor): item is DataAccessor<'armor'> =>
        item.type === 'armor' && item.system.equipped
    )
  }

  get talents(): DataAccessor<TalentItemType>[] {
    return ['talent', 'combatTalent', 'language', 'scripture']
      .map((type) => this.itemTypes[type] as DataAccessor<TalentItemType>)
      .flat()
  }

  get spells(): DataAccessor<'spell'>[] {
    return this.itemTypes['spell'] as DataAccessor<'spell'>[]
  }

  get weapons(): DataAccessor<WeaponItemType>[] {
    return ['meleeWeapon', 'rangedWeapon']
      .map((type) => this.itemTypes[type] as DataAccessor<WeaponItemType>)
      .flat()
  }

  get shields(): DataAccessor<'shield'>[] {
    return this.itemTypes['shield'] as DataAccessor<'shield'>[]
  }

  private combatStateItem(
    combatStateAttribute: 'primaryHand' | 'secondaryHand' | 'unarmedTalent'
  ): DsaItem | undefined {
    const itemdId = this.system.base.combatState[combatStateAttribute]
    return itemdId ? this.items.get(itemdId) : undefined
  }

  get combatState(): CombatState {
    const primaryHandItem = this.combatStateItem('primaryHand')
    const primaryHand =
      primaryHandItem !== undefined &&
      (primaryHandItem.type === 'meleeWeapon' ||
        primaryHandItem.type === 'rangedWeapon')
        ? (primaryHandItem as DataAccessor<WeaponItemType>)
        : undefined

    const secondaryHandItem = this.combatStateItem('secondaryHand')
    const secondaryHand = secondaryHandItem
      ? secondaryHandItem.type === 'shield'
        ? (secondaryHandItem as DataAccessor<'shield'>)
        : (secondaryHandItem as DataAccessor<WeaponItemType>)
      : undefined

    const unarmedTalentItem = this.combatStateItem('unarmedTalent')
    const unarmedTalent = unarmedTalentItem
      ? (unarmedTalentItem as DataAccessor<'combatTalent'>)
      : undefined

    return {
      isArmed: this.system.base.combatState.isArmed,
      primaryHand,
      secondaryHand,
      unarmedTalent,
    }
  }

  //Compability to handlebars sheet
  rangedAttackModifier(weaponData, options) {
    const weapon = this.items.find((item) => item.id == weaponData._id)

    if (weapon) {
      const game = getGame()
      return game.ruleset.compute(ComputeRangedAttack, {
        character: new Character(this),
        weapon: GenericWeapon.create(weapon as DataAccessor<WeaponItemType>),
        ...options,
      }).mod
    }
  }

  isEquipable(weapon) {
    return this.talent(weapon.system.talent) !== undefined
  }

  rollDamage(weapon, isFastForward = false, options) {
    const game = getGame()
    const name = game.i18n.localize('DSA.damage')
    const character = new Character(this)
    let damageOptions: MeleeCombatOptions | undefined = undefined
    const weaponInstance = GenericWeapon.create(weapon)
    if (weaponInstance != null) {
      damageOptions = { weapon: weaponInstance }
    }
    const parts = [character.damage(damageOptions), options.mod]

    return new DsaRoll(parts, { name }).roll(isFastForward)
  }

  async rollAttribute(
    abilityId,
    isFastForward = false,
    isNegativeAttribute = false,
    options
  ) {
    let name = ''
    let targetValue = 0

    if (isNegativeAttribute) {
      const attribute = this.items.get(abilityId)
      name = attribute?.name || ''
      targetValue =
        (attribute?.type === 'disadvantage' && attribute.system.value) || 0
    } else {
      const game = getGame()
      name = game.i18n.localize('DSA.' + abilityId)
      targetValue = this.system.base.basicAttributes[abilityId].value
    }

    const parts = ['1d20', options.mod]

    const rollOptions = {
      successChecker: (roll) => roll.total <= targetValue,
      criticalChecker: critical1d20,
      name,
    }

    return new DsaRoll(parts, rollOptions).roll(isFastForward)
  }

  rollCombatAction(action, itemData, options) {
    const character = new Character(this)
    const weapon =
      itemData !== undefined &&
      ['meleeWeapon', 'rangedWeapon'].includes(itemData.type)
        ? GenericWeapon.create(itemData)
        : undefined
    const talent =
      itemData !== undefined && itemData.type === 'combatTalent'
        ? new GenericCombatTalent(itemData, character)
        : undefined

    character[action]({ ...options, weapon, talent })
  }

  rollSkill(skillId, options) {
    const items = this.items.filter((i) => i.id === skillId)

    if (items.length === 0) return

    const skillData = items[0]
    TestableSkill.create(
      skillData as DataAccessor<TestableSkillItemType>,
      new Character(this)
    )?.roll(options)
  }

  async importItem(data) {
    const game = getGame()

    if (data.pack) {
      const pack = game.packs.get(data.pack)
      if (pack) {
        const item = await pack.get(data.id)
        return await this.createEmbeddedDocuments('Item', [item?.system])
      }
    } else if (data.data) {
      return this.createEmbeddedDocuments('Item', [data.system])
    } else {
      const game = getGame()
      const item = game.items?.get(data.id)
      if (item) {
        return this.createEmbeddedDocuments('Item', [item.system])
      }
    }
    return undefined
  }

  prepareBaseData(): void {
    super.prepareBaseData()
    // We have to make sure that there is an object in the array so that foundry detects the object type
    this.system.additionalItems = [{ name: 'dummy' }]
  }

  generateTemporaryItems(): void {
    const tempItemIDs = this.items
      .filter((item) => item.pack === 'effect')
      .map((item) => item.id)
    tempItemIDs.forEach(
      (id) => id && this.items.delete(id, { modifySource: false })
    )
    this.tempItemPromise = new Promise((resolve) => resolve(undefined))
    for (const additionalItem of this.system.additionalItems) {
      if (additionalItem.uuid === undefined) continue
      this.tempItemPromise = this.tempItemPromise.then(() =>
        Item.fromDropData(additionalItem as any).then((item) => {
          if (item !== undefined && item.id !== null) {
            if (
              item.system.sid === undefined ||
              item.system.sid === '' ||
              !this.items.some((i) => i.system.sid === item.system.sid)
            ) {
              // const id = foundry.utils.randomID()
              const tempItem = new Item(item.toObject(), { pack: 'effect' })
              if (additionalItem.value) {
                tempItem.system.value = additionalItem.value
              }

              this.items.set(item.id, tempItem, { modifySource: false })
            }
          }
        })
      )
    }
  }

  prepareDerivedData(): void {
    super.prepareDerivedData()
    this.generateTemporaryItems()
    const character = new Character(this)
    const game = getGame()
    const derivedDataConfiguration = [
      {
        path: 'system.base.combatAttributes.active.baseAttack.value',
        computation: ComputeBaseAttack,
        enabled: this.system.settings.autoCalcBaseAttack,
      },
      {
        path: 'system.base.combatAttributes.active.baseParry.value',
        computation: ComputeBaseParry,
        enabled: this.system.settings.autoCalcBaseParry,
      },
      {
        path: 'system.base.combatAttributes.active.baseRangedAttack.value',
        computation: ComputeBaseRangedAttack,
        enabled: this.system.settings.autoCalcBaseRangedAttack,
      },
      {
        path: 'system.base.combatAttributes.active.baseInitiative.value',
        computation: ComputeBaseInitiative,
        enabled: this.system.settings.autoCalcInitiative,
      },
      {
        path: 'system.base.combatAttributes.passive.magicResistance.value',
        computation: ComputeMagicResistance,
        enabled: this.system.settings.autoCalcMagicResistance,
      },
    ]

    derivedDataConfiguration.forEach(
      (conf) =>
        conf.enabled &&
        setByPath(
          this,
          conf.path,
          game.ruleset.compute(conf.computation, { character })
        )
    )

    const derivedData = derivedDataConfiguration.map((conf) => conf.path)
    this.applyDerivedDataActiveEffects(derivedData)
  }

  private applyDerivedDataActiveEffects(derivedData: string[]) {
    const overrides = {}

    // Organize non-disabled effects by their application priority
    const changes = this.effects.reduce((changes: EffectChangeData[], e) => {
      if (e.disabled || e.isSuppressed) return changes
      return changes
        .concat(
          e.changes.map((c) => {
            c = foundry.utils.duplicate(c)
            c.effect = e
            c.priority = c.priority ?? c.mode * 10
            return c
          })
        )
        .filter((change) => derivedData.includes(change.key))
    }, [])
    changes.sort((a, b) => a.priority - b.priority)

    // Apply all changes
    for (const change of changes) {
      const result = change.effect.apply(this, change)
      if (result !== null) overrides[change.key] = result
    }

    // Expand the set of final overrides
    this.overrides = foundry.utils.expandObject({
      ...foundry.utils.flattenObject(this.overrides),
      ...overrides,
    })
  }
}

function setByPath(obj: any, path: string, value: any) {
  const parts = path.split('.')
  return parts.reduce(
    (prev, curr, ix) =>
      ix + 1 == parts.length
        ? (prev[curr] = value)
        : (prev[curr] = prev[curr] || {}),
    obj
  )
}
