import { AttributeName, CharacterData } from '../model/character-data.js'
import {
  GeneralNamedAttribute,
  NamedAttribute,
  Rollable,
} from '../model/properties.js'
import { RollAttribute } from '../ruleset/rules/basic-roll-mechanic.js'
import type { Ruleset } from '../ruleset/ruleset.js'
import { Character } from './character.js'

export class Attribute implements Rollable<NamedAttribute> {
  private data_: CharacterData
  private ruleset: Ruleset
  name: AttributeName

  constructor(data: CharacterData, name: AttributeName, ruleset?: Ruleset) {
    this.data_ = data
    this.name = name
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = (<any>game).ruleset
    }
  }

  get value(): number {
    return this.data_.base.basicAttributes[this.name].value
  }

  roll(options: any): void {
    this.ruleset.execute(RollAttribute, {
      attributeName: this.name,
      targetValue: this.value,
      ...options,
    })
  }
}

export class NegativeAttribute implements Rollable<GeneralNamedAttribute> {
  private character: Character
  private ruleset: Ruleset
  private sid: string
  name: string

  constructor(character: Character, sid: string, ruleset?: Ruleset) {
    this.character = character
    this.sid = sid
    this.name = this.character.data.disadvantage(this.sid)?.name || ''
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = (<any>game).ruleset
    }
  }

  get value(): number {
    return this.character.data.disadvantage(this.sid)?.system.value || 0
  }

  roll(options: any): void {
    this.ruleset.execute(RollAttribute, {
      attributeName: this.name,
      targetValue: this.value,
      isLocalized: true,
      ...options,
    })
  }
}
