// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace DSADie {
  interface Modifiers {
    cc: 'checkCritical'
  }
}

export class DSADie extends Die {
  secondRoll?: number

  constructor(termData) {
    termData.faces = 20
    super(termData)
  }

  // get formula(): string {
  //   return '1W20'
  // }

  checkCritical(modifier): DSADie | undefined {
    const rgx = /[cC][cC]([<>=]+)?([0-9]+)?/
    const match = modifier.match(rgx)
    if (!match) return this

    const n = this.results.length
    if (n !== 1) return this
    const firstRoll = this.results[0]

    if (firstRoll.result === 1 || firstRoll.result === 20) {
      firstRoll.exploded = true
      this.roll()
      const secondRoll = this.results[1]
      secondRoll.active = false
      if (firstRoll.result === 1) firstRoll.success = true
      if (firstRoll.result === 20) firstRoll.failure = true
      this.secondRoll = secondRoll.result
    }
  }

  static DENOMINATION = 'z'
  static MODIFIERS = {
    ...Die.MODIFIERS,
    cc: 'checkCritical',
  }
}

// DiceTerm.matchTerm = (expression) => {
//   const rgx = new RegExp(
//     `^([0-9]+)?[dDwW]([A-z]|[0-9]+)${DiceTerm.MODIFIERS_REGEX}${DiceTerm.FLAVOR_TEXT_REGEX}`
//   )
//   const match = expression.match(rgx)
//   return match || null
// }
