import { BaseDataAccessor, DataTypeMap, ItemType } from '../model/item-data.js'

Hooks.on('createItem', async (item) => {
  if (item.system.sid === '') {
    // const itemData = item.data
    await item.update({
      system: {
        sid: item.id,
      },
    })
  }
})

Hooks.on('preCreateItem', (newItem) => {
  if (newItem.isOwned) {
    const actor = newItem.parent
    if (newItem.system?.isUniquelyOwnable && newItem.system?.sid != '') {
      const isAlreadyOwned = [...actor.items.values()].some(
        (item) => newItem.system.sid === item.system.sid
      )
      if (isAlreadyOwned) {
        ui.notifications?.info('Item is already equipped!', {})
      }
      return !isAlreadyOwned
    }
  }
})

interface DataSource<typeName extends ItemType> {
  type: typeName
  data: DataTypeMap[typeName]
  system: this['data']
}

type DataSourceUnion<Types extends ItemType> = {
  [K in Types]: DataSource<K>
}[Types]

type ItemDataSource = DataSourceUnion<ItemType>
type ItemDataProperties = ItemDataSource

declare global {
  interface Item {
    system: this['data']['system']
  }
  interface DocumentClassConfig {
    Item: typeof DsaItem
  }
  interface SourceConfig {
    Item: ItemDataSource
  }

  interface DataConfig {
    Item: ItemDataProperties
  }
}

export class DsaItem extends Item implements BaseDataAccessor {}
