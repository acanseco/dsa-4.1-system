import { DsaItemSheet } from './item-sheet.js'
import {
  TalentType,
  TalentCategory,
  Attribute,
  CombatTalentCategory,
  EffectiveEncumbaranceType,
} from '../enums.js'
import { labeledEnumValues } from '../i18n.js'

export class TalentSheet extends DsaItemSheet {
  async getData() {
    const data = await super.getData()

    data.talentTypes = labeledEnumValues(TalentType)
    data.talentCategories = labeledEnumValues(TalentCategory).filter(
      (category) =>
        category.value != TalentCategory.Combat &&
        category.value != TalentCategory.Language
    )

    data.data.effectiveEncumbaranceTypes = labeledEnumValues(
      EffectiveEncumbaranceType
    )

    return data
  }
}

export class LinguisticTalentSheet extends DsaItemSheet {
  get template() {
    return 'systems/dsa-41/templates/items/linguisticTalent-sheet.html'
  }
}

function enrichLinguisticItem(document) {
  if (document.type === 'language' || document.type === 'scripture') {
    const updateData = {
      system: {
        type: TalentType.Special,
        category: TalentCategory.Language,
        test: {},
      },
    }

    switch (document.type) {
      case 'language':
        updateData.system.test = {
          firstAttribute: Attribute.Cleverness,
          secondAttribute: Attribute.Intuition,
          thirdAttribute: Attribute.Charisma,
        }
        break
      case 'scripture':
        updateData.system.test = {
          firstAttribute: Attribute.Cleverness,
          secondAttribute: Attribute.Cleverness,
          thirdAttribute: Attribute.Dexterity,
        }
        break
    }
    document.updateSource(updateData)
  }
}

Hooks.on('preCreateItem', (document) => {
  enrichLinguisticItem(document)
})

export class CombatTalentSheet extends TalentSheet {
  async getData() {
    const data = await super.getData()
    data.combatTalentCategories = labeledEnumValues(CombatTalentCategory)

    return data
  }
}
