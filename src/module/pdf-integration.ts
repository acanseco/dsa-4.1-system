import { RuleBook } from './enums.js'
import { getGame } from './utils.js'

const BookCodes = {
  [RuleBook.LiberCantionesDeluxe]: 'LCD',
  [RuleBook.LiberLiturgium]: 'LL',
}

const PageAttributes = {
  [RuleBook.LiberCantionesDeluxe]: 'lcdPage',
  [RuleBook.LiberLiturgium]: 'llPage',
}

const PageOffsetSetting = {
  [RuleBook.LiberCantionesDeluxe]: 'lcd-page-offset',
  [RuleBook.LiberLiturgium]: 'll-page-offset',
}

export function openPDFPage(book, page) {
  const game = getGame()
  const pageOffset = game.settings.get('dsa-41', PageOffsetSetting[book])
  const bookJournal = game.journal?.find((j) => j.name == BookCodes[book])
  if (bookJournal) {
    const pdfSheetId = bookJournal?.pages.contents[0].id
    const pdfPageSheet = bookJournal?.sheet?.getPageSheet(pdfSheetId)
    pdfPageSheet.page = page + pageOffset
    pdfPageSheet.autoLoadPDF = true
    bookJournal.sheet?.render(true, { collapsed: true })
  }
}

export function createOpenPDFPageListener(book) {
  return (event) => {
    const page = parseInt($(event.currentTarget).data(PageAttributes[book]))
    openPDFPage(book, page)
  }
}

Hooks.on('renderJournalPDFPageSheet', (sheet, html) => {
  if (sheet.autoLoadPDF) {
    sheet.loadPDF(html)
  }
})

export class EnhancedJournalPDFPageSheet extends JournalPDFPageSheet {
  page: number
  autoLoadPDF = false

  constructor(doc: any, options: any) {
    super(doc, options)
  }

  _onLoadPDF(event) {
    const target = event.currentTarget.parentElement
    this.loadPDF(target)
  }

  loadPDF(target) {
    const frame = document.createElement('iframe')
    frame.src = `scripts/pdfjs/web/viewer.html?${this._getViewerParams()}#${this._getAdditionalViewerParams()}`
    target.replaceWith(frame)
  }

  _getAdditionalViewerParams() {
    const params = new URLSearchParams()
    if (this.page) {
      params.append('page', String(this.page))
    }
    return params
  }
}
