import type { Ruleset } from '../ruleset.js'
import {
  BaseComputationOptionData,
  CreateComputationIdentifier,
} from '../rule-components.js'
import { liturgyDegrees } from '../../model/properties.js'
import { DescribeRule } from '../rule.js'

interface DegreeModifierOptionData extends BaseComputationOptionData {
  degree: string
}

export const ComputeDegreeModifier = CreateComputationIdentifier<
  DegreeModifierOptionData,
  number
>('degreeMod')

export const BasicKarmaRule = DescribeRule(
  'karma-modifier-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .on(ComputeDegreeModifier)
      .do((options: DegreeModifierOptionData): number => {
        const index = liturgyDegrees.indexOf(options.degree)
        if (index < 0) return 0

        return index * 2 - 2
      })
  }
)
