import { CombatAction } from './basic-combat.js'
import type { CombatActionData, CombatActionResult } from './basic-combat.js'
import type { Ruleset } from '../ruleset.js'
import { CreateActionIdentifier } from '../rule-components.js'
import {
  ComputeRangedAttack,
  RangeClass,
  SizeClass,
} from './derived-combat-attributes.js'
import type {
  CombatComputationResult,
  RangedCombatComputationData,
} from './derived-combat-attributes.js'
import {
  RollAttributeToChatEffect,
  totalModifier,
} from './basic-roll-mechanic.js'
import { DescribeRule } from '../rule.js'
import type { ModifierDescriptor } from '../../model/modifier.js'

type ModifierTable<Class extends string | number | symbol> = Record<
  Class,
  number
>

const RangeModificators: ModifierTable<RangeClass> = {
  [RangeClass.VeryNear]: -2,
  [RangeClass.Near]: 0,
  [RangeClass.Medium]: 4,
  [RangeClass.Far]: 8,
  [RangeClass.VeryFar]: 12,
}

const SizeModificators: ModifierTable<SizeClass> = {
  [SizeClass.Tiny]: 8,
  [SizeClass.VerySmall]: 6,
  [SizeClass.Small]: 4,
  [SizeClass.Medium]: 2,
  [SizeClass.Big]: 0,
  [SizeClass.VeryBig]: -2,
}

interface Enum {
  [key: number]: string | number
}

function updateModifierByMap<Class extends number>(
  e: Enum,
  map: ModifierTable<Class>,
  key: Class | undefined,
  options: RangedCombatComputationData,
  result?: CombatComputationResult
): CombatComputationResult | RangedCombatComputationData {
  if (options.modifiers === undefined) {
    options.modifiers = []
  }
  const uncapitalize = (s: string) => s && s[0].toLowerCase() + s.slice(1)

  if (key) {
    if (map[key]) {
      options.modifiers.push({
        name: uncapitalize(`${e[key]}`),
        mod: map[key],
        modifierType: 'other',
      })
    }
  }
  const totalMod = totalModifier(options.modifiers)
  if (result) {
    result.mod = totalMod
    return result
  }
  options.mod = totalMod
  return options
}

function RangeModification<
  ResultType extends CombatComputationResult | RangedCombatComputationData
>(
  options: RangedCombatComputationData,
  result?: CombatComputationResult
): ResultType {
  return updateModifierByMap(
    RangeClass,
    RangeModificators,
    options.rangeClass,
    options,
    result
  ) as ResultType
}

function SizeModification<
  ResultType extends CombatComputationResult | RangedCombatComputationData
>(
  options: RangedCombatComputationData,
  result?: CombatComputationResult
): ResultType {
  return updateModifierByMap(
    SizeClass,
    SizeModificators,
    options.sizeClass,
    options,
    result
  ) as ResultType
}

export interface RangedCombatActionData extends CombatActionData {
  rangeClass?: RangeClass
  sizeClass?: SizeClass
}

export const RangedAttackAction = CreateActionIdentifier<
  RangedCombatActionData,
  CombatActionResult<RangedCombatActionData>
>('rangedAttack')

export const BasicRangedCombatRule = DescribeRule(
  'basic-ranged-combat-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.after(ComputeRangedAttack).do(RangeModification)
    ruleset.after(ComputeRangedAttack).do(SizeModification)
    ruleset.on(RangedAttackAction).do(CombatAction)
    ruleset.after(RangedAttackAction).trigger(RollAttributeToChatEffect)
    ruleset.before(RangedAttackAction).do(RangeModification)
    ruleset.before(RangedAttackAction).do(SizeModification)
  }
)
