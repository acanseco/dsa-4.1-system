import type { Ruleset } from '../ruleset.js'
import {
  BaseComputationOptionData,
  CreateComputationIdentifier,
} from '../rule-components.js'
import { DescribeRule } from '../rule.js'
import { AttributeName } from '../../model/character-data.js'
import type { BaseCharacter } from '../../model/character.js'

export const ComputeBaseAttack = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('baseAttack')

export const ComputeBaseParry = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('baseParry')

export const ComputeBaseRangedAttack = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('baseRangedAttack')

export const ComputeBaseInitiative = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('baseInitiative')

export const ComputeMagicResistance = CreateComputationIdentifier<
  BaseComputationOptionData,
  number
>('magicResistance')

export const DerivedAttributesRule = DescribeRule(
  'derived-attributes-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .on(ComputeBaseAttack)
      .do(computeDerivedValue(['courage', 'agility', 'strength'], 5))

    ruleset
      .on(ComputeBaseParry)
      .do(computeDerivedValue(['intuition', 'agility', 'strength'], 5))

    ruleset
      .on(ComputeBaseRangedAttack)
      .do(computeDerivedValue(['intuition', 'dexterity', 'strength'], 5))

    ruleset
      .on(ComputeBaseInitiative)
      .do(
        computeDerivedValue(['courage', 'courage', 'intuition', 'agility'], 5)
      )

    ruleset
      .on(ComputeMagicResistance)
      .do(computeDerivedValue(['courage', 'cleverness', 'constitution'], 5))
  }
)

function computeDerivedValue(attributes: AttributeName[], denominator: number) {
  return (options: BaseComputationOptionData) =>
    derivedValue(options.character, attributes, denominator)
}

function derivedValue(
  character: BaseCharacter,
  attributes: AttributeName[],
  denominator: number
) {
  return Math.round(
    attributes.reduce(
      (prev: number, attribute: AttributeName) =>
        prev + character.data.attribute(attribute).value,
      0
    ) / denominator
  )
}
