import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'
import { createManeuverRule } from './maneuver-creator.js'

export const Bind: SpecialAbility = {
  name: 'Binden',
  identifier: 'ability-binden',
}

export const BindManeuver = new Maneuver('bind', 'defensive', {
  minMod: 4,
})

export const BindRule = createManeuverRule(Bind, BindManeuver)
