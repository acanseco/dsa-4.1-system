import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Holdup: SpecialAbility = {
  name: 'Entwaffnen',
  identifier: 'ability-entwaffnen',
}

export const HoldupManeuver = new Maneuver('holdup', 'defensive')
export const HoldupRule = createManeuverRule(Holdup, HoldupManeuver)
