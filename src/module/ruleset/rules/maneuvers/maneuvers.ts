import { BindRule } from './bind.js'
import { ClearanceRule } from './clearance.js'
import { DefensiveStyleRule } from './defensive-style.js'
import { DisarmRule } from './disarm.js'
import { DownthrowRule } from './downthrow.js'
import { FeintRule } from './feint.js'
import { HammerStrikeRule } from './hammer-strike.js'
import { HoldupRule } from './holdup.js'
import { MasterparryRule } from './masterparry.js'
import { MightyStrikeRule } from './mighty-strike.js'
import { SallyRule } from './sally.js'

export const ManeuverRules = [
  BindRule,
  ClearanceRule,
  DefensiveStyleRule,
  DisarmRule,
  DownthrowRule,
  FeintRule,
  HammerStrikeRule,
  HoldupRule,
  MasterparryRule,
  MightyStrikeRule,
  SallyRule,
]
