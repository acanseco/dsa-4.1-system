import type { SpecialAbility } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import { ComputeInitiativeFormula } from '../basic-combat.js'
import type { ComputeInitiativeFormulaData } from '../basic-combat.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'

export const BladeDancer: SpecialAbility = {
  name: 'Klingentänzer',
  identifier: 'ability-klingentaenzer',
}
export const BladeDancerRule = DescribeRule(
  'bladeDancer',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .before(ComputeInitiativeFormula)
      .when(CharacterHas(BladeDancer))
      .do((options: ComputeInitiativeFormulaData) => ({
        ...options,
        dices: 2,
      }))
  }
)
