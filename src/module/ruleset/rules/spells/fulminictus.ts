import type { SkillDescriptor } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import type { Ruleset } from '../../ruleset.js'
import { SpellAction } from '../basic-skill.js'
import type { SkillActionData, SkillActionResult } from '../basic-skill.js'

export const Fulminictus: SkillDescriptor = {
  name: 'Fulminictus',
  identifier: 'spell-fulminictus',
  skillType: 'spell',
}

function usesSpell(spell: SkillDescriptor) {
  return (options: SkillActionData, _result?: SkillActionResult): boolean => {
    return options.skill.identifier === spell.identifier
  }
}

export const FulminictusRule = DescribeRule(
  'fulminictus',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(SpellAction)
      .when(usesSpell(Fulminictus))
      .do((_options, result) => {
        result.damage = '2d6 + ' + result.roll.total

        return result
      })
  }
)
