import type { MeleeWeapon } from '../../model/items.js'
import { DescribeRule } from '../rule.js'
import type { Ruleset } from '../ruleset.js'
import { usesMeleeWeapon } from './basic-combat.js'
import { ComputeDamageFormula } from './derived-combat-attributes.js'

export const WeaponStrengthModifierRule = DescribeRule(
  'weapon-strength-modifier',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeDamageFormula)
      .when(usesMeleeWeapon)
      .do((options, result) => {
        const strengthDiff =
          options.character.attribute('strength').value -
          (<MeleeWeapon>options.weapon).strengthMod.threshold
        const damageMod =
          Math.sign(strengthDiff) *
          Math.floor(
            Math.abs(strengthDiff) /
              (<MeleeWeapon>options.weapon).strengthMod.hitPointStep
          )
        result.bonusDamage += damageMod
        return result
      })
  }
)
