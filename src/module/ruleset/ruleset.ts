import { CreateRule, Rule } from './rule.js'
import type { RuleConfigManager, RuleCreator, RuleDescriptor } from './rule.js'
import { Computation, Action, Effect, ChatEffect } from './rule-components.js'
import type {
  PostHook,
  PreHook,
  BaseComputationOptionData,
  RuleManager,
  ComputationIdentifier,
  ActionIdentifier,
  Func,
  EffectIdentifier,
  ChatEffectListenedResult,
  BaseActionOptionData,
  BaseActionResultType,
  BaseEffectOptionData,
  AmendedRuleComponentIdentifier,
  ListenedResultTypeOf,
} from './rule-components.js'

export type Predicate<OptionData, ResultType = unknown> = (
  options: OptionData,
  result?: ResultType
) => boolean

function TruePredicate<OptionData, ResultType>(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _options: OptionData,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _result?: ResultType
): boolean {
  return true
}

function ComposePredicates<OptionData, ResultType = unknown>(
  predicate1: Predicate<OptionData, ResultType>,
  predicate2: Predicate<OptionData, ResultType>,
  composer: (a: boolean, b: boolean) => boolean = (a, b) => a && b
): Predicate<OptionData, ResultType> {
  return (options: OptionData, result?: ResultType) =>
    composer(predicate1(options, result), predicate2(options, result))
}

export function Is<OptionData, ResultType = unknown>(
  bool: boolean
): Predicate<OptionData, ResultType> {
  return (_options: OptionData, _result?: ResultType) => bool
}

export function Not<OptionData, ResultType = unknown>(
  predicate1: Predicate<OptionData, ResultType>
): Predicate<OptionData, ResultType> {
  return (options: OptionData, result?: ResultType) =>
    !predicate1(options, result)
}

export function And<OptionData, ResultType = unknown>(
  predicate1: Predicate<OptionData, ResultType>,
  predicate2: Predicate<OptionData, ResultType>
): Predicate<OptionData, ResultType> {
  return ComposePredicates(predicate1, predicate2)
}

export function Or<OptionData, ResultType = unknown>(
  predicate1: Predicate<OptionData, ResultType>,
  predicate2: Predicate<OptionData, ResultType>
): Predicate<OptionData, ResultType> {
  return ComposePredicates(predicate1, predicate2, (a, b) => a || b)
}

type Constructor<T> = new (...args: any[]) => T

interface BaseRuleComponentDescriber<ArgType> {
  do: (a: ArgType) => void
}
interface RuleComponentDescriber<ArgType, OptionData, ResultType>
  extends BaseRuleComponentDescriber<ArgType> {
  when: (
    predicate: Predicate<OptionData, ResultType>
  ) => RuleComponentDescriber<ArgType, OptionData, ResultType>
}

interface RuleComponentDescriberWithTrigger<ArgType, OptionData, ResultType>
  extends RuleComponentDescriber<ArgType, OptionData, ResultType> {
  trigger: <ListenedResultType>(
    effectIdentifier: EffectIdentifier<ListenedResultType>
  ) => void
}

export class Ruleset implements RuleManager {
  private computations: Map<string, Computation>
  private actions: Map<string, Action<any, any>>
  private effects: Map<string, Effect<any>>

  private resultListeners: Map<string, EffectIdentifier<any>[]>
  private configManager: RuleConfigManager
  private rules: Rule[]

  constructor(configManager: RuleConfigManager) {
    this.computations = new Map<string, Computation>()
    this.actions = new Map<string, Action<any, any>>()
    this.effects = new Map<string, Effect<any>>()

    this.resultListeners = new Map<string, EffectIdentifier<any>[]>()

    this.registerEffect<ChatEffectListenedResult, ChatEffect>(new ChatEffect())

    this.configManager = configManager
    this.rules = []
  }

  registerComputation(computation: Computation): void {
    this.computations.set(computation.identifier.name, computation)
  }

  registerComputationPreHook<
    OptionData extends BaseComputationOptionData,
    ResultType
  >(
    identifier: ComputationIdentifier<OptionData, ResultType>,
    hook: PreHook<OptionData>
  ): void {
    this.computations.get(identifier.name)?.addPreHook(hook)
  }

  registerComputationPostHook<
    OptionData extends BaseComputationOptionData,
    ResultType
  >(
    identifier: ComputationIdentifier<OptionData, ResultType>,
    hook: PostHook<OptionData, ResultType>
  ): void {
    this.computations.get(identifier.name)?.addPostHook(hook)
  }

  registerAction<
    ActionOptionData extends BaseActionOptionData,
    ActionResultType extends BaseActionResultType<ActionOptionData>,
    A extends Action<ActionOptionData, ActionResultType>
  >(action: A): void {
    this.actions.set(action.identifier.name, action)
    this.resultListeners.set(action.identifier.name, [])
    action.ruleset = this
  }

  registerActionPreHook<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>
  >(
    identifier: ActionIdentifier<OptionData, ResultType>,
    hook: PreHook<OptionData>
  ): void {
    this.actions.get(identifier.name)?.addPreHook(hook)
  }

  registerActionPostHook<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>
  >(
    identifier: ActionIdentifier<OptionData, ResultType>,
    hook: PostHook<OptionData, Promise<ResultType>>
  ): void {
    this.actions.get(identifier.name)?.addPostHook(hook)
  }

  registerEffect<ListenedResult, E extends Effect<ListenedResult>>(
    effect: E
  ): void {
    this.effects.set(effect.identifier.name, effect)
  }

  registerEffectPreHook<ListenedResultType>(
    identifier: EffectIdentifier<ListenedResultType>,
    hook: PreHook<BaseEffectOptionData<ListenedResultType>>
  ): void {
    this.effects.get(identifier.name)?.addPreHook(hook)
  }

  registerActionResultListener<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>
  >(
    actionIdentifier: ActionIdentifier<OptionData, ResultType>,
    effectIdentifier: EffectIdentifier<ResultType>
  ): void {
    this.resultListeners.get(actionIdentifier.name)?.push(effectIdentifier)
  }

  compute<OptionData extends BaseComputationOptionData, ResultType>(
    identifier: ComputationIdentifier<OptionData, ResultType>,
    options: OptionData
  ): ResultType {
    return this.computations.get(identifier.name)?.execute(options)
  }

  async execute<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>
  >(
    identifier: ActionIdentifier<OptionData, ResultType>,
    options: OptionData
  ): Promise<ResultType> {
    const result = await this.actions.get(identifier.name)?.execute(options)
    for (const effectIdentifier of this.resultListeners?.get(identifier.name) ||
      []) {
      const effect = this.effects.get(effectIdentifier.name)
      if (effect) {
        await effect.apply(result)
      }
    }
    return result
  }

  on<OptionData extends BaseComputationOptionData, ResultType>(
    identifier: ComputationIdentifier<OptionData, ResultType>
  ): BaseRuleComponentDescriber<
    Func<OptionData | BaseComputationOptionData, ResultType>
  >
  on<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>
  >(
    identifier: ActionIdentifier<OptionData, ResultType>
  ): BaseRuleComponentDescriber<Constructor<Action<OptionData, ResultType>>>

  on<
    OptionData extends BaseComputationOptionData & BaseActionOptionData,
    ResultType
  >(
    identifier: AmendedRuleComponentIdentifier<OptionData, ResultType>
  ):
    | BaseRuleComponentDescriber<
        Func<OptionData | BaseComputationOptionData, ResultType>
      >
    | BaseRuleComponentDescriber<
        Constructor<
          Action<OptionData, ResultType & BaseActionResultType<OptionData>>
        >
      >
    | BaseRuleComponentDescriber<undefined> {
    switch (identifier.type) {
      case 'computation':
        return {
          do: (
            func: Func<OptionData | BaseComputationOptionData, ResultType>
          ) => {
            this.registerComputation(new Computation(identifier, func))
          },
        }
      case 'action':
        return {
          do: <
            ConcreteAction extends Action<
              OptionData,
              ResultType & BaseActionResultType<OptionData>
            >
          >(
            actionClass: Constructor<ConcreteAction>
          ) => {
            this.registerAction(new actionClass(identifier))
          },
        }
    }
    return {
      do: () => {
        throw new Error('unknown identifier')
      },
    }
  }

  before<OptionData extends BaseComputationOptionData, ResultType>(
    identifier: ComputationIdentifier<OptionData, ResultType>
  ): RuleComponentDescriber<PreHook<OptionData>, OptionData, ResultType>
  before<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>
  >(
    identifier: ActionIdentifier<OptionData, ResultType>
  ): RuleComponentDescriber<PreHook<OptionData>, OptionData, ResultType>
  before<ListenedResultType>(
    identifier: EffectIdentifier<ListenedResultType>
  ): RuleComponentDescriber<
    PreHook<BaseEffectOptionData<ListenedResultType>>,
    BaseEffectOptionData<ListenedResultType>,
    void
  >

  before<
    OptionData extends BaseComputationOptionData &
      BaseActionOptionData &
      BaseEffectOptionData<ListenedResultType>,
    ResultType,
    ListenedResultType = null
  >(
    identifier:
      | ComputationIdentifier<OptionData, ResultType>
      | ActionIdentifier<
          OptionData,
          ResultType & BaseActionResultType<OptionData>
        >
      | EffectIdentifier<ListenedResultType>
  ): RuleComponentDescriber<PreHook<OptionData>, OptionData, ResultType> {
    return this.beforeWhen<OptionData, ResultType, ListenedResultType>(
      identifier
    )
  }

  private registerPreHook<
    OptionData extends BaseComputationOptionData &
      BaseActionOptionData &
      BaseEffectOptionData<ListenedResultType>,
    ResultType,
    ListenedResultType = null
  >(
    identifier: AmendedRuleComponentIdentifier<OptionData, ResultType>,
    hook: PreHook<OptionData>
  ): void {
    switch (identifier.type) {
      case 'action':
        this.registerActionPreHook(
          <
            ActionIdentifier<
              OptionData,
              ResultType & BaseActionResultType<OptionData>
            >
          >identifier,
          hook
        )
        break
      case 'computation':
        this.registerComputationPreHook(
          <
            ComputationIdentifier<
              OptionData & BaseComputationOptionData,
              ResultType
            >
          >identifier,
          hook
        )
        break
      case 'effect':
        this.registerEffectPreHook(
          <EffectIdentifier<ListenedResultTypeOf<OptionData>>>identifier,
          hook
        )
        break
    }
  }

  private beforeWhen<
    OptionData extends BaseComputationOptionData &
      BaseActionOptionData &
      BaseEffectOptionData<ListenedResultType>,
    ResultType,
    ListenedResultType = null
  >(
    identifier: AmendedRuleComponentIdentifier<OptionData, ResultType>,
    predicate: Predicate<OptionData, ResultType> = TruePredicate
  ): RuleComponentDescriber<PreHook<OptionData>, OptionData, ResultType> {
    return {
      do: (hook: PreHook<OptionData>) => {
        const conditionalHook = (options) =>
          predicate(options) ? hook(options) : options
        this.registerPreHook(identifier, conditionalHook)
      },
      when: (
        innerPredicate: Predicate<OptionData, ResultType>
      ): RuleComponentDescriber<PreHook<OptionData>, OptionData, ResultType> =>
        this.beforeWhen<OptionData, ResultType, ListenedResultType>(
          identifier,
          ComposePredicates(predicate, innerPredicate)
        ),
    }
  }

  after<OptionData extends BaseComputationOptionData, ResultType>(
    identifier: ComputationIdentifier<OptionData, ResultType>
  ): RuleComponentDescriberWithTrigger<
    PostHook<OptionData, ResultType>,
    OptionData,
    ResultType
  >
  after<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>
  >(
    identifier: ActionIdentifier<OptionData, ResultType>
  ): RuleComponentDescriberWithTrigger<
    PostHook<OptionData, ResultType>,
    OptionData,
    ResultType
  >

  after<
    OptionData extends BaseComputationOptionData & BaseActionOptionData,
    ResultType
  >(
    identifier:
      | ComputationIdentifier<OptionData, ResultType>
      | ActionIdentifier<
          OptionData,
          ResultType & BaseActionResultType<OptionData>
        >
  ): RuleComponentDescriberWithTrigger<
    PostHook<OptionData, ResultType>,
    OptionData,
    ResultType
  > {
    return this.afterWhen(identifier)
  }

  private registerPostHook<
    OptionData extends BaseComputationOptionData &
      BaseActionOptionData &
      BaseEffectOptionData<ListenedResultType>,
    ResultType,
    ListenedResultType = null
  >(
    identifier: AmendedRuleComponentIdentifier<OptionData, ResultType>,
    hook: PostHook
  ): void {
    switch (identifier.type) {
      case 'action':
        this.registerActionPostHook(
          <
            ActionIdentifier<
              OptionData,
              ResultType & BaseActionResultType<OptionData>
            >
          >identifier,
          hook
        )
        break
      case 'computation':
        this.registerComputationPostHook(
          <ComputationIdentifier<OptionData, ResultType>>identifier,
          hook
        )
        break
    }
  }

  private afterWhen<
    OptionData extends BaseComputationOptionData & BaseActionOptionData,
    ResultType
  >(
    identifier: AmendedRuleComponentIdentifier<OptionData, ResultType>,
    predicate: Predicate<OptionData, ResultType> = TruePredicate
  ): RuleComponentDescriberWithTrigger<
    PostHook<OptionData, ResultType>,
    OptionData,
    ResultType
  > {
    return {
      do: (hook: PostHook<OptionData, ResultType>) => {
        const conditionalHook = (options: OptionData, result: ResultType) =>
          predicate(options, result) ? hook(options, result) : result
        this.registerPostHook(identifier, conditionalHook)
      },
      trigger: <ListenedResultType>(
        effectIdentifier: EffectIdentifier<ListenedResultType>
      ) =>
        this.registerActionResultListener(
          <
            ActionIdentifier<
              OptionData,
              ResultType & BaseActionResultType<OptionData>
            >
          >identifier,
          effectIdentifier
        ),
      when: (
        innerPredicate: Predicate<OptionData, ResultType>
      ): RuleComponentDescriber<
        PostHook<OptionData, ResultType>,
        OptionData,
        ResultType
      > =>
        this.afterWhen<OptionData, ResultType>(
          identifier,
          ComposePredicates<OptionData, ResultType>(predicate, innerPredicate)
        ),
    }
  }

  add(descriptor: RuleDescriptor, creator: RuleCreator = CreateRule): void {
    const rule = creator(descriptor)
    rule.createConfig(this.configManager)
    rule.loadConfig(this.configManager)
    this.rules.push(rule)
  }

  compileRules(): void {
    this.rules.forEach((rule) => rule.loadInto(this))
  }
}
