import { SvelteApplication } from '@typhonjs-fvtt/runtime/svelte/application'
import DialogShell from './DialogShell.svelte'

export default class SvelteDialog extends SvelteApplication {
  constructor(options = {}) {
    super(options)
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      svelte: {
        class: DialogShell,
        target: document.body,
        props: {},
      },
    })
  }
}
