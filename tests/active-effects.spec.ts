class ActiveEffectConfig {
  async getData(_options) {
    return {
      effect: {
        changes: [],
      },
    }
  }
}
global.ActiveEffectConfig = ActiveEffectConfig

import { DsaActiveEffectConfig } from '../src/module/active-effect.js'

describe('DsaActiveEffectConfig', () => {
  test('to use the correct template', () => {
    const config = new DsaActiveEffectConfig()
    expect(config.template).toEqual(
      `systems/dsa-41/templates/active-effect.html`
    )
  })

  test.each([
    ['system.base.movement.speed.value', 'speed'],
    [
      'system.base.combatAttributes.passive.magicResistance.value',
      'magicResistance',
    ],
    ['system.base.combatAttributes.active.baseAttack.value', 'DSA.baseAttack'],
    ['system.base.combatAttributes.active.baseParry.value', 'DSA.baseParry'],
    [
      'system.base.combatAttributes.active.baseRangedAttack.value',
      'DSA.baseRangedAttack',
    ],
    [
      'system.base.combatAttributes.active.baseInitiative.value',
      'DSA.baseInitiative',
    ],
    ['system.base.combatAttributes.active.dodge.value', 'DSA.dodge'],
    ['system.base.basicAttributes.courage.value', 'DSA.courage'],
    ['system.base.basicAttributes.cleverness.value', 'DSA.cleverness'],
    ['system.base.basicAttributes.intuition.value', 'DSA.intuition'],
    ['system.base.basicAttributes.charisma.value', 'DSA.charisma'],
    ['system.base.basicAttributes.dexterity.value', 'DSA.dexterity'],
    ['system.base.basicAttributes.agility.value', 'DSA.agility'],
    ['system.base.basicAttributes.constitution.value', 'DSA.constitution'],
    ['system.base.basicAttributes.strength.value', 'DSA.strength'],
  ])('to set all required stats as options', async (option, label) => {
    const config = new DsaActiveEffectConfig()
    const data = await config.getData({})
    expect(data).toEqual(
      expect.objectContaining({
        changeableAttributes: expect.objectContaining({
          [option]: label,
        }),
      })
    )
  })
})
