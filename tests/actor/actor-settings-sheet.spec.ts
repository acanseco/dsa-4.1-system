class MockTJSDocument {
  public doc: any
  set(doc) {
    this.doc = doc
  }
}

class MockSvelteApplication {
  close() {
    return
  }
}

class MockActorSettingsSheetShellSvelte {}

vi.mock('@typhonjs-fvtt/runtime/svelte/store', () => ({
  TJSDocument: MockTJSDocument,
}))
vi.mock('@typhonjs-fvtt/runtime/svelte/application', () => ({
  SvelteApplication: MockSvelteApplication,
}))
vi.mock('../../src/ui/actor-settings/ActorSettingsSheetShell.svelte', () => ({
  default: MockActorSettingsSheetShellSvelte,
}))

global.foundry = {
  utils: {
    mergeObject: (org, other) => other,
  },
}

const { ActorSettingsSheet } = await import(
  '../../src/module/actor/actor-settings-sheet.js'
)

describe('ActorSettingsSheet', () => {
  it('to return wanted default options', () => {
    const options = ActorSettingsSheet.defaultOptions

    expect(options).toMatchObject<any>({
      title: 'actorSettings',
      svelte: expect.objectContaining({
        class: MockActorSettingsSheetShellSvelte,
        target: document.body,
      }),
    })
  })

  it('to provide svelte store as prop', () => {
    const doc = { test: 'test' }
    const sheet = new ActorSettingsSheet(doc)
    const options = ActorSettingsSheet.defaultOptions
    expect(options.svelte.props.bind(sheet)().doc.doc).toEqual(doc)
  })

  it('to provide localizer as prop', () => {
    const doc = { test: 'test' }
    const sheet = new ActorSettingsSheet(doc)
    const options = ActorSettingsSheet.defaultOptions
    expect(options.svelte.props.bind(sheet)().localize('abc')).toEqual('abc')
  })
})
