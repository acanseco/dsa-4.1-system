import type {
  CharacterSettings,
  CharacterBaseData,
} from '../../src/module/model/character-data.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

const Hooks = {
  on: vi.fn(),
}
global.Hooks = Hooks

class Item {}
global.Item = Item

class Actor {
  prepareDerivedData() {
    return
  }
}
global.Actor = Actor

global.foundry = {
  utils: {
    mergeObject: (org, other) => other,
    expandObject: vi.fn(),
    flattenObject: vi.fn(),
  },
}

const ruleset = createMockRuleset()
import { getGame } from '../../src/module/utils.js'
getGame.mockReturnValue({ ruleset })

ruleset.compute.mockReturnValue(1)

const { DsaActor } = await import('../../src/module/actor/actor.js')

describe('DsaActor', () => {
  it('to calculate derived data if enabled', () => {
    const settings: CharacterSettings = {
      autoCalcBaseAttack: true,
      autoCalcBaseParry: true,
      autoCalcBaseRangedAttack: true,
      autoCalcInitiative: true,
      autoCalcMagicResistance: true,
    }
    const actor = new DsaActor({
      name: 'test',
      type: 'character',
    })
    actor.system = {
      base: {} as CharacterBaseData,
      settings: settings as CharacterSettings,
      additionalItems: [],
    }
    actor.items = []
    actor.effects = []
    actor.prepareDerivedData()
    expect(actor.system.base.combatAttributes.active.baseAttack.value).toEqual(
      1
    )
    expect(actor.system.base.combatAttributes.active.baseParry.value).toEqual(1)
    expect(
      actor.system.base.combatAttributes.active.baseRangedAttack.value
    ).toEqual(1)
    expect(
      actor.system.base.combatAttributes.active.baseInitiative.value
    ).toEqual(1)
    expect(
      actor.system.base.combatAttributes.passive.magicResistance.value
    ).toEqual(1)
  })

  it('to not calculate derived data if disabled', () => {
    const settings: CharacterSettings = {
      autoCalcBaseAttack: false,
      autoCalcBaseParry: false,
      autoCalcBaseRangedAttack: false,
      autoCalcInitiative: false,
      autoCalcMagicResistance: false,
    }
    const actor = new DsaActor({
      name: 'test',
      type: 'character',
    })
    actor.system = {
      base: {
        combatAttributes: {
          active: {
            baseAttack: {
              value: 0,
            },
            baseParry: {
              value: 0,
            },
            baseRangedAttack: {
              value: 0,
            },
            baseInitiative: {
              value: 0,
            },
          },
          passive: {
            magicResistance: {
              value: 0,
            },
          },
        } as any,
      } as CharacterBaseData,
      settings: settings as CharacterSettings,
      additionalItems: [],
    }
    actor.items = []
    actor.effects = []
    actor.prepareDerivedData()
    expect(actor.system.base.combatAttributes.active.baseAttack.value).toEqual(
      0
    )
    expect(actor.system.base.combatAttributes.active.baseParry.value).toEqual(0)
    expect(
      actor.system.base.combatAttributes.active.baseRangedAttack.value
    ).toEqual(0)
    expect(
      actor.system.base.combatAttributes.active.baseInitiative.value
    ).toEqual(0)
    expect(
      actor.system.base.combatAttributes.passive.magicResistance.value
    ).toEqual(0)
  })
})
