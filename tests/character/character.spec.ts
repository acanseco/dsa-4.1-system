import { when } from 'jest-when'
import { Character } from '../../src/module/character/character.js'
import { GenericShield } from '../../src/module/character/shield.js'
import { Skill } from '../../src/module/character/skill.js'
import { GenericWeapon } from '../../src/module/character/weapon.js'
import type { CharacterDataAccessor } from '../../src/module/model/character.js'
import type {
  DataAccessor,
  TalentItemType,
  WeaponItemType,
} from '../../src/module/model/item-data.js'
import type { Armor, Shield, Weapon } from '../../src/module/model/items.js'
import type {
  ManeuverType,
  ModifierDescriptor,
} from '../../src/module/model/modifier.js'
import type {
  BaseProperty,
  BaseTalent,
  SpecialAbility,
  CombatTalent,
} from '../../src/module/model/properties.js'
import {
  AttackAction,
  ComputeInitiativeFormula,
  DodgeAction,
  ParryAction,
} from '../../src/module/ruleset/rules/basic-combat.js'
import { RangedAttackAction } from '../../src/module/ruleset/rules/basic-ranged-combat.js'
import {
  ComputeArmorClass,
  ComputeAttack,
  ComputeDamageFormula,
  ComputeEffectiveEncumbarance,
  ComputeEncumbarance,
  ComputeParry,
  ComputeRangedAttack,
  RangeClass,
  SizeClass,
} from '../../src/module/ruleset/rules/derived-combat-attributes.js'
import { ComputeManeuverList } from '../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

describe('Character', function () {
  const name = 'Testname'
  const baseAttack = 12
  const baseParry = 12
  const baseRangedAttack = 12
  const baseInitiative = 9
  const dodge = 12
  const strength = 14
  const talentSID = 'talent-swords'
  const talentName = 'Swords'
  const talent: BaseTalent = {
    name: talentName,
    identifier: talentSID,
  } as BaseTalent
  const talentData: DataAccessor<TalentItemType> = {
    name: talentName,
    system: {
      sid: talentSID,
      value: 0,
    },
  } as DataAccessor<TalentItemType>

  const specialAbilityName = 'Wuchtschlag'
  const specialAbilitySID = 'ability-wuchtschlag'
  const specialAbility: BaseProperty = {
    name: specialAbilityName,
    identifier: specialAbilitySID,
  }
  const specialAbilityData: DataAccessor<'specialAbility'> = {
    name: specialAbilityName,
    system: {
      sid: specialAbilitySID,
    },
  } as DataAccessor<'specialAbility'>

  const armorName = 'Kettenhemd'
  const armorClass = 3
  const armorEncumbarance = 2
  const armorItems: DataAccessor<'armor'>[] = []
  armorItems.push({
    name: armorName,
    system: {
      armorClass,
      encumbarance: armorEncumbarance,
      equipped: true,
    },
  } as DataAccessor<'armor'>)

  armorItems.push({
    name: 'another' + armorName,
    system: {
      armorClass: armorClass + 3,
      encumbarance: armorEncumbarance + 2,
      equipped: false,
    },
  } as DataAccessor<'armor'>)

  const combatStateData = {
    isArmed: true,
    primaryHand: {} as DataAccessor<WeaponItemType>,
    secondaryHand: {} as DataAccessor<WeaponItemType | 'shield'>,
    unarmedTalent: {} as DataAccessor<'combatTalent'>,
  }

  const dataAccessor: CharacterDataAccessor = {
    combatState: combatStateData,
    properties: [specialAbilityData, talentData],

    name,
    system: {
      base: {
        resources: {},
        movement: {
          speed: {
            value: 0,
            unit: 'Schritt',
          },
        },
        combatState: {
          isArmed: false,
          primaryHand: undefined,
          secondaryHand: undefined,
          unarmedTalent: undefined,
        },
        basicAttributes: {
          strength: {
            value: strength,
          },
        },
        combatAttributes: {
          active: {
            baseAttack: {
              value: baseAttack,
            },
            baseParry: {
              value: baseParry,
            },
            baseRangedAttack: {
              value: baseRangedAttack,
            },
            baseInitiative: {
              value: baseInitiative,
            },
            dodge: {
              value: dodge,
            },
          },
          passive: {},
        },
      },
    },
    armorItems,
    update: vi.fn(),
    attribute: vi.fn(),
    baseAttack: 0,
    baseParry: 0,
    baseRangedAttack: 0,
    baseInitiative: 0,
    dodge: 0,
    talent: vi.fn(),
    spell: vi.fn(),
    liturgy: vi.fn(),
    specialAbilities: [],
    specialAbility: vi.fn(),
  }

  const ruleset = createMockRuleset()

  it('should provide an interface to the data provider', function () {
    const stubDataProvider: CharacterDataAccessor = {
      name: 'Testname',
    } as CharacterDataAccessor
    const character = new Character(stubDataProvider, ruleset)
    expect(character.data).toEqual(stubDataProvider)
  })

  it('should provide the strength attribute of the given dataAccessor', function () {
    const character = new Character(dataAccessor, ruleset)

    expect(character.attribute('strength').value).toEqual(strength)
  })

  it('should provide the characters base attack value', function () {
    const character = new Character(dataAccessor, ruleset)

    const value = 12
    dataAccessor.system.base.combatAttributes.active.baseAttack.value = value

    const result = character.baseAttack
    expect(result).toEqual(value)
  })
  it('should provide the characters base parry value', function () {
    const character = new Character(dataAccessor, ruleset)

    const value = 12
    dataAccessor.system.base.combatAttributes.active.baseParry.value = value

    const result = character.baseParry
    expect(result).toEqual(value)
  })
  it('should provide the characters base ranged attack value', function () {
    const character = new Character(dataAccessor, ruleset)

    const value = 12
    dataAccessor.system.base.combatAttributes.active.baseRangedAttack.value =
      value

    const result = character.baseRangedAttack
    expect(result).toEqual(value)
  })
  it('should provide the characters base initiative value', function () {
    const character = new Character(dataAccessor, ruleset)

    const value = 12
    dataAccessor.baseInitiative = value

    const result = character.baseInitiative
    expect(result).toEqual(value)
  })
  it('should provide the characters base dodge value', function () {
    const character = new Character(dataAccessor, ruleset)

    const value = 12
    dataAccessor.system.base.combatAttributes.active.dodge.value = value

    const result = character.baseDodge
    expect(result).toEqual(value)
  })

  it('should provide the characters attack value for a weapon', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {} as Weapon

    const value = 12
    when(ruleset.compute)
      .expectCalledWith(
        ComputeAttack,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({ value })

    const result = character.attackValue({ weapon })
    expect(result).toEqual(value)
  })

  it('should provide the characters attack value for a weapon via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const weaponItem = {
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    combatStateData.primaryHand = weaponItem

    const weapon = GenericWeapon.create(weaponItem)

    const value = 12
    when(ruleset.compute)
      .expectCalledWith(
        ComputeAttack,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({ value })

    const result = character.attackValue()
    expect(result).toEqual(value)
  })

  it('should provide the characters attack value for a weapon and a shield', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {} as Weapon
    const shield = {} as Shield

    const value = 12
    when(ruleset.compute)
      .expectCalledWith(
        ComputeAttack,
        expect.objectContaining({ weapon, shield, character })
      )
      .mockReturnValue({ value })

    const result = character.attackValue({ weapon, shield })
    expect(result).toEqual(value)
  })

  it('should provide the characters attack value for a weapon and a shield via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const weaponItem = {
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const shieldItem = {
      type: 'shield',
    } as DataAccessor<'shield'>
    combatStateData.primaryHand = weaponItem
    combatStateData.secondaryHand = shieldItem

    const weapon = GenericWeapon.create(weaponItem)
    const shield = new GenericShield(shieldItem)

    const value = 12
    when(ruleset.compute)
      .expectCalledWith(
        ComputeAttack,
        expect.objectContaining({ weapon, shield, character })
      )
      .mockReturnValue({ value })

    const result = character.attackValue()
    expect(result).toEqual(value)
  })

  it('should provide the characters attack value for combat with two weapons for the second weapon', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {
      name: 'FirstWeapon',
    } as Weapon
    const secondaryWeapon = {
      name: 'SecondWeapon',
    } as Weapon
    const useSecondaryHand = true

    const value = 12
    when(ruleset.compute)
      .expectCalledWith(
        ComputeAttack,
        expect.objectContaining({ weapon: secondaryWeapon, character })
      )
      .mockReturnValue({ value })

    const result = character.attackValue({
      weapon,
      secondaryWeapon,
      useSecondaryHand,
    })
    expect(result).toEqual(value)
  })

  it('should provide the characters attack value for combat with two weapons for the second weapon via combat state', function () {
    const character = new Character(dataAccessor, ruleset)

    const weaponItem = {
      name: 'FirstWeapon',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const secondaryWeaponItem = {
      name: 'SecondWeapon',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const useSecondaryHand = true
    combatStateData.primaryHand = weaponItem
    combatStateData.secondaryHand = secondaryWeaponItem

    const secondaryWeapon = GenericWeapon.create(secondaryWeaponItem)

    const value = 12
    when(ruleset.compute)
      .expectCalledWith(
        ComputeAttack,
        expect.objectContaining({ weapon: secondaryWeapon, character })
      )
      .mockReturnValue({ value })

    const result = character.attackValue(useSecondaryHand)
    expect(result).toEqual(value)
  })

  it('should provide the characters attack value for an unarmed attack', function () {
    const character = new Character(dataAccessor, ruleset)
    const talent = {} as CombatTalent

    const value = 12
    when(ruleset.compute)
      .expectCalledWith(
        ComputeAttack,
        expect.objectContaining({ talent, character })
      )
      .mockReturnValue({ value })

    const result = character.attackValue({ talent })
    expect(result).toEqual(value)
  })

  it('should provide the characters attack value for an unarmed attack via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const talentItem = {} as DataAccessor<'combatTalent'>
    combatStateData.unarmedTalent = talentItem
    combatStateData.isArmed = false

    const talent = Skill.create(talentItem, character, ruleset)

    const value = 12
    when(ruleset.compute)
      .expectCalledWith(
        ComputeAttack,
        expect.objectContaining({ talent, character })
      )
      .mockReturnValue({ value })

    const result = character.attackValue()
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for a weapon', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {} as Weapon

    const value = 13
    when(ruleset.compute)
      .expectCalledWith(
        ComputeParry,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({ value })

    const result = character.parryValue({ weapon })
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for a weapon via combat state', function () {
    const character = new Character(dataAccessor, ruleset)

    const weaponItem = {
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    combatStateData.primaryHand = weaponItem
    const weapon = GenericWeapon.create(weaponItem)

    const value = 13
    when(ruleset.compute)
      .expectCalledWith(
        ComputeParry,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({ value })

    const result = character.parryValue()
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for a weapon and a shield', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {} as Weapon
    const shield = {} as Shield

    const value = 13
    when(ruleset.compute)
      .expectCalledWith(
        ComputeParry,
        expect.objectContaining({ weapon, shield, character })
      )
      .mockReturnValue({ value })

    const result = character.parryValue({ weapon, shield })
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for a weapon and a shield via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const weaponItem = {
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const shieldItem = {
      type: 'shield',
    } as DataAccessor<'shield'>
    combatStateData.primaryHand = weaponItem
    combatStateData.secondaryHand = shieldItem
    const weapon = GenericWeapon.create(weaponItem)
    const shield = new GenericShield(shieldItem)

    const value = 13
    when(ruleset.compute)
      .expectCalledWith(
        ComputeParry,
        expect.objectContaining({ weapon, shield, character })
      )
      .mockReturnValue({ value })

    const result = character.parryValue()
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for combat with two weapons for the second weapon', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {
      name: 'FirstWeapon',
    } as Weapon
    const secondaryWeapon = {
      name: 'SecondWeapon',
    } as Weapon
    const useSecondaryHand = true

    const value = 13
    when(ruleset.compute)
      .expectCalledWith(
        ComputeParry,
        expect.objectContaining({ weapon: secondaryWeapon, character })
      )
      .mockReturnValue({ value })

    const result = character.parryValue({
      weapon,
      secondaryWeapon,
      useSecondaryHand,
    })
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for combat with two weapons for the second weapon via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const weaponItem = {
      name: 'FirstWeapon',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const secondaryWeaponItem = {
      name: 'SecondWeapon',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const useSecondaryHand = true

    const secondaryWeapon = GenericWeapon.create(secondaryWeaponItem)
    combatStateData.primaryHand = weaponItem
    combatStateData.secondaryHand = secondaryWeaponItem

    const value = 13
    when(ruleset.compute)
      .expectCalledWith(
        ComputeParry,
        expect.objectContaining({ weapon: secondaryWeapon, character })
      )
      .mockReturnValue({ value })

    const result = character.parryValue(useSecondaryHand)
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for an unarmed parry', function () {
    const character = new Character(dataAccessor, ruleset)
    const talent = {} as CombatTalent

    const value = 13
    when(ruleset.compute)
      .expectCalledWith(
        ComputeParry,
        expect.objectContaining({ talent, character })
      )
      .mockReturnValue({ value })

    const result = character.parryValue({ talent })
    expect(result).toEqual(value)
  })

  it('should provide the characters parry value for an unarmed parry via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const talentItem = {} as DataAccessor<'combatTalent'>
    const talent = Skill.create(talentItem, character, ruleset)
    combatStateData.unarmedTalent = talentItem
    combatStateData.isArmed = false

    const value = 13
    when(ruleset.compute)
      .expectCalledWith(
        ComputeParry,
        expect.objectContaining({ talent, character })
      )
      .mockReturnValue({ value })

    const result = character.parryValue()
    expect(result).toEqual(value)
  })

  it('should provide the characters ranged attack value for a weapon', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {} as Weapon

    const value = 19

    when(ruleset.compute)
      .expectCalledWith(
        ComputeRangedAttack,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({ value })

    const result = character.rangedAttackValue({ weapon })
    expect(result).toEqual(value)
  })

  it('should provide the characters ranged attack value for a weapon via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const weaponItem = {
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const weapon = GenericWeapon.create(weaponItem)
    combatStateData.primaryHand = weaponItem

    const value = 19

    when(ruleset.compute)
      .expectCalledWith(
        ComputeRangedAttack,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({ value })

    const result = character.rangedAttackValue()
    expect(result).toEqual(value)
  })

  it('should provide the characters damage formula for a weapon', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {} as Weapon
    const formula = '1d6+3'

    when(ruleset.compute)
      .expectCalledWith(
        ComputeDamageFormula,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({
        baseDamage: null,
        bonusDamage: 0,
        multiplier: 0,
        formula,
      })

    const result = character.damage({ weapon })
    expect(result).toEqual(formula)
  })

  it('should provide the characters damage formula for a weapon via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const weaponItem = {
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const weapon = GenericWeapon.create(weaponItem)
    combatStateData.primaryHand = weaponItem
    const formula = '1d6+3'

    when(ruleset.compute)
      .expectCalledWith(
        ComputeDamageFormula,
        expect.objectContaining({ weapon, character })
      )
      .mockReturnValue({
        baseDamage: null,
        bonusDamage: 0,
        multiplier: 0,
        formula,
      })

    const result = character.damage()
    expect(result).toEqual(formula)
  })

  it('should provide the characters damage formula of the secondary weapon is requested', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {
      name: 'FirstWeapon',
    } as Weapon
    const secondaryWeapon = {
      name: 'FirstWeapon',
    } as Weapon
    const formula = '1d6+3'
    const useSecondaryHand = true

    when(ruleset.compute)
      .expectCalledWith(
        ComputeDamageFormula,
        expect.objectContaining({ weapon: secondaryWeapon, character })
      )
      .mockReturnValue({
        baseDamage: null,
        bonusDamage: 0,
        multiplier: 0,
        formula,
      })

    const result = character.damage({
      weapon,
      secondaryWeapon,
      useSecondaryHand,
    })
    expect(result).toEqual(formula)
  })

  it('should provide the characters damage formula of the secondary weapon is requested via combat state', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {
      name: 'FirstWeapon',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const secondaryWeapon = {
      name: 'SecondWeapon',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    const useSecondaryHand = true

    combatStateData.primaryHand = weapon
    combatStateData.secondaryHand = secondaryWeapon

    const formula = '1d6+3'

    when(ruleset.compute)
      .expectCalledWith(
        ComputeDamageFormula,
        expect.objectContaining({
          weapon: GenericWeapon.create(secondaryWeapon),
          character,
        })
      )
      .mockReturnValue({
        baseDamage: null,
        bonusDamage: 0,
        multiplier: 0,
        formula,
      })

    const result = character.damage(useSecondaryHand)
    expect(result).toEqual(formula)
  })

  it('should provide the characters equipped armor items as a list', function () {
    const armorDataItem = {
      name: armorName,
      system: {
        armorClass: armorClass,
        encumbarance: armorEncumbarance,
      },
    }
    const armor = {
      name: armorDataItem.name,
      ...armorDataItem.system,
    }
    const stubDataProvider: CharacterDataAccessor = {
      armorItems: [armorDataItem],
    } as CharacterDataAccessor
    const character = new Character(stubDataProvider, ruleset)
    const expected: Armor[] = [armor]

    const result = character.armorItems
    expect(result).toEqual(expected)
  })

  it('should provide the characters encumbarance value', function () {
    const character = new Character(dataAccessor, ruleset)

    const value = 3
    when(ruleset.compute)
      .calledWith(ComputeEncumbarance, expect.objectContaining({ character }))
      .mockReturnValue(value)

    const result = character.encumbarance
    expect(result).toEqual(value)
  })

  test.each([
    ['BE', 2],
    ['BE–1', 1],
    ['BE–3', 0],
    ['BE+1', 3],
    ['BEx2', 4],
  ])(
    'should be able to compute the effective encumbarance based on a formula',
    function (formula: string, expectedEncumbarance: number) {
      const character = new Character(dataAccessor, ruleset)

      when(ruleset.compute)
        .calledWith(
          ComputeEffectiveEncumbarance,
          expect.objectContaining({ character, formula })
        )
        .mockReturnValue(expectedEncumbarance)

      const result = character.effectiveEncumbarance(formula)

      expect(result).toEqual(expectedEncumbarance)
    }
  )

  it('should provide the characters armor class value', function () {
    const character = new Character(dataAccessor, ruleset)

    const value = 3
    when(ruleset.compute)
      .calledWith(ComputeArmorClass, expect.objectContaining({ character }))
      .mockReturnValue(value)

    const result = character.armorClass
    expect(result).toEqual(value)
  })

  it('should be able to check if a ability is present', function () {
    const character = new Character(dataAccessor, ruleset)

    expect(character.has(specialAbility)).toEqual(true)
  })

  it('should be able to check if a talent is present', function () {
    const character = new Character(dataAccessor, ruleset)

    expect(character.has(talent)).toEqual(true)
  })

  it('should be able to check if an ability is not present', function () {
    const character = new Character(dataAccessor, ruleset)
    const otherAbility: SpecialAbility = {
      name: 'OtherAbility',
      identifier: 'ability-other',
    }

    expect(character.has(otherAbility)).toEqual(false)
  })

  const baseCombatActions = ['attack', 'parry', 'rangedAttack']
  const actionIdentifiers = {
    attack: AttackAction,
    parry: ParryAction,
    rangedAttack: RangedAttackAction,
  }

  test.each(baseCombatActions)(
    'should execute a base combat action',
    function (actionName: string) {
      const character = new Character(dataAccessor, ruleset)
      const weapon = {} as Weapon
      const actionIdentifier = actionIdentifiers[actionName]

      character[actionName]({ weapon })
      expect(ruleset.execute).toHaveBeenCalledWith(
        actionIdentifier,
        expect.objectContaining({ weapon, character })
      )
    }
  )

  test.each(baseCombatActions)(
    'should execute a base combat action with modifiers',
    function (actionName: string) {
      const character = new Character(dataAccessor, ruleset)
      const weapon = {} as Weapon
      const actionIdentifier = actionIdentifiers[actionName]

      const modifiers: ModifierDescriptor[] = [
        {
          name: 'test',
          mod: 4,
          modifierType: 'other',
        },
      ]

      character[actionName]({ weapon, modifiers })
      expect(ruleset.execute).toHaveBeenCalledWith(
        actionIdentifier,
        expect.objectContaining({ weapon, character, modifiers })
      )
    }
  )

  test.each(baseCombatActions.filter((action) => action !== 'rangedAttack'))(
    'should execute a base combat action with a shield',
    function (actionName: string) {
      const character = new Character(dataAccessor, ruleset)
      const weapon = {} as Weapon
      const shield = {} as Shield
      const actionIdentifier = actionIdentifiers[actionName]

      character[actionName]({ weapon, shield })
      expect(ruleset.execute).toHaveBeenCalledWith(
        actionIdentifier,
        expect.objectContaining({ weapon, character, shield })
      )
    }
  )

  it('should execute a ranged attack action with size and range class', function () {
    const character = new Character(dataAccessor, ruleset)
    const weapon = {} as Weapon
    const sizeClass = SizeClass.Tiny
    const rangeClass = RangeClass.Near

    character.rangedAttack({ weapon, sizeClass, rangeClass })
    expect(ruleset.execute).toHaveBeenCalledWith(
      RangedAttackAction,
      expect.objectContaining({ weapon, character, sizeClass, rangeClass })
    )
  })

  it('should execute a dodge action', function () {
    const character = new Character(dataAccessor, ruleset)

    character.dodge()
    expect(ruleset.execute).toHaveBeenCalledWith(
      DodgeAction,
      expect.objectContaining({ character })
    )
  })

  it('should provide requested talent', function () {
    const character = new Character(dataAccessor, ruleset)
    const sid = 'talent-suchen'
    const talentData = {
      name: 'Suchen',
      type: 'talent',
    }
    when(dataAccessor.talent).calledWith(sid).mockReturnValue(talentData)
    const talent = character.talent(sid)
    expect(talent?.name).toEqual(talentData.name)
  })

  it('should provide requested spell', function () {
    const character = new Character(dataAccessor, ruleset)
    const sid = 'spell-gardianum'
    const spellData = {
      name: 'Gardianum',
      type: 'spell',
    }
    when(dataAccessor.spell).calledWith(sid).mockReturnValue(spellData)
    const spell = character.spell(sid)
    expect(spell?.name).toEqual(spellData.name)
  })

  it('should provide requested liturgy', function () {
    const character = new Character(dataAccessor, ruleset)
    const sid = 'liturgy-beten'
    const liturgyData = {
      name: 'Beten',
      type: 'liturgy',
    }
    when(dataAccessor.liturgy).calledWith(sid).mockReturnValue(liturgyData)
    const liturgy = character.liturgy(sid)
    expect(liturgy?.name).toEqual(liturgyData.name)
  })

  it('should provide karma talent', function () {
    const character = new Character(dataAccessor, ruleset)
    const sid = 'talent-liturgiekenntnis'
    const talentData = {
      name: 'Liturgiekenntnis',
      type: 'talent',
    }
    when(dataAccessor.talent).calledWith(sid).mockReturnValue(talentData)
    const talent = character.karmaTalent()
    expect(talent?.name).toEqual(talentData.name)
  })

  test.each(['offensive', 'defensive'])(
    'should provide the available maneuvers for the characters of the given maneuver type',
    function (maneuverType: ManeuverType) {
      const character = new Character(dataAccessor, ruleset)
      const maneuvers: ModifierDescriptor[] = [
        {
          name: 'TestManeuver',
          modifierType: 'maneuver',
          mod: 0,
        },
      ]

      when(ruleset.compute)
        .calledWith(
          ComputeManeuverList,
          expect.objectContaining({ character, maneuverType })
        )
        .mockReturnValue({ maneuvers })

      const result = character.maneuverList(maneuverType)
      expect(result).toEqual(maneuvers)
    }
  )

  it('should be able to roll initiative if unarmed', function () {
    combatStateData.isArmed = false

    const expected = '1d6 + 9'

    const character = new Character(dataAccessor, ruleset)
    when(ruleset.compute)
      .expectCalledWith(
        ComputeInitiativeFormula,
        expect.objectContaining({ character })
      )
      .mockReturnValue({ formula: expected })

    const result = character.initiative().formula
    expect(result).toEqual(expected)
  })

  it('should be able to roll initiative if unarmed and uses an unarmed combat talent', function () {
    combatStateData.isArmed = false
    combatStateData.unarmedTalent = {
      name: 'Raufen',
      system: {
        category: 'unarmed',
      },
    } as DataAccessor<'combatTalent'>
    combatStateData.primaryHand = {
      name: 'Dolch',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>

    const expected = '1d6 + 9'

    const character = new Character(dataAccessor, ruleset)
    when(ruleset.compute)
      .expectCalledWith(
        ComputeInitiativeFormula,
        expect.objectContaining({
          character,
          talent: Skill.create(combatStateData.unarmedTalent, character),
          weapon: undefined,
          shield: undefined,
        })
      )
      .mockReturnValue({ formula: expected })

    const result = character.initiative().formula
    expect(result).toEqual(expected)
  })

  it('should be able to roll initiative if armed and uses a weapon', function () {
    combatStateData.isArmed = true
    combatStateData.unarmedTalent = {
      name: 'Raufen',
      system: {
        category: 'unarmed',
      },
    } as DataAccessor<'combatTalent'>
    combatStateData.primaryHand = {
      name: 'Dolch',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>

    const expected = '1d6 + 9'

    const character = new Character(dataAccessor, ruleset)
    when(ruleset.compute)
      .expectCalledWith(
        ComputeInitiativeFormula,
        expect.objectContaining({
          character,
          talent: undefined,
          weapon: GenericWeapon.create(combatStateData.primaryHand),
          shield: undefined,
        })
      )
      .mockReturnValue({ formula: expected })

    const result = character.initiative().formula
    expect(result).toEqual(expected)
  })

  it('should be able to roll initiative if armed and uses a weapon and a shield', function () {
    combatStateData.isArmed = true
    combatStateData.unarmedTalent = {
      name: 'Raufen',
      system: {
        category: 'unarmed',
      },
    } as DataAccessor<'combatTalent'>
    combatStateData.primaryHand = {
      name: 'Dolch',
      type: 'meleeWeapon',
    } as DataAccessor<'meleeWeapon'>
    combatStateData.secondaryHand = {
      name: 'Schild',
      type: 'shield',
    } as DataAccessor<'shield'>

    const expected = '1d6 + 9'

    const character = new Character(dataAccessor, ruleset)
    when(ruleset.compute)
      .expectCalledWith(
        ComputeInitiativeFormula,
        expect.objectContaining({
          character,
          talent: undefined,
          weapon: GenericWeapon.create(combatStateData.primaryHand),
          shield: new GenericShield(
            combatStateData.secondaryHand as DataAccessor<'shield'>
          ),
        })
      )
      .mockReturnValue({ formula: expected })

    const result = character.initiative().formula
    expect(result).toEqual(expected)
  })

  beforeEach(() => {
    combatStateData.isArmed = true
    combatStateData.primaryHand = {} as DataAccessor<WeaponItemType>
    combatStateData.secondaryHand = {} as DataAccessor<
      WeaponItemType | 'shield'
    >
    combatStateData.unarmedTalent = {} as DataAccessor<'combatTalent'>
  })

  afterEach(() => {
    vi.clearAllMocks()
    ruleset.execute.mockReset()
    ruleset.compute.mockReset()
  })
})
