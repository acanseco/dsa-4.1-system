import { GenericSpell } from '../../src/module/character/skill.js'
import type { BaseCharacter } from '../../src/module/model/character.js'
import type { DataAccessor } from '../../src/module/model/item-data.js'
import type { TestAttributes } from '../../src/module/model/properties.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

describe('GenericSpell', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  const spellValue = 12
  const spellName = 'Ignifaxius'
  const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
  const itemData = {
    name: spellName,
    type: 'spell',
    system: {
      value: spellValue,
      test: {
        firstAttribute: testAttributes[0],
        secondAttribute: testAttributes[1],
        thirdAttribute: testAttributes[2],
      },
    },
  } as DataAccessor<'spell'>
  const spell = new GenericSpell(itemData, character, ruleset)

  it('should have its given name', function () {
    expect(spell.name).toEqual(spellName)
  })

  it('should be of skill type spell', function () {
    expect(spell.skillType).toEqual('spell')
  })

  it('should return the correct value of its related data entity', function () {
    expect(spell.value).toEqual(spellValue)
  })

  it('should provide a list of test attributes', function () {
    expect(spell.testAttributes[0]).toEqual(testAttributes[0])
    expect(spell.testAttributes[1]).toEqual(testAttributes[1])
    expect(spell.testAttributes[2]).toEqual(testAttributes[2])
  })
})
