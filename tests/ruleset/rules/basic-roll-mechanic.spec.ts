import {
  BasicRollMechanicRule,
  RollAttribute,
  RollCombatAttribute,
  RollAttributeToChatEffect,
  RollSkill,
  SkillRollChatEffect,
  RollSkillToChatEffect,
  AttributeRollChatEffect,
  BasicRoll,
  BasicRollChatEffect,
  BasicRollActionResult,
  BasicRollToChatEffect,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic.js'
import type {
  RollFactory,
  Rollable,
  Messagable,
  SpeakerProvider,
  RollMessageCreator,
  Localizer,
  SkillRollActionData,
  AttributeActionResult,
  MessageData,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic.js'
import { TestAction } from '../test-classes.js'
import { when } from 'jest-when'
import type { Ruleset } from '../../../src/module/ruleset/ruleset.js'
import { createTestRuleset } from './helpers.js'
import type { SkillActionResult } from '../../../src/module/ruleset/rules/basic-skill.js'
import {
  BaseActionOptionData,
  CreateActionIdentifier,
} from '../../../src/module/ruleset/rule-components.js'
import type { BaseCharacter } from '../../../src/module/model/character.js'
import { AttributeName } from '../../../src/module/model/character-data.js'

describe('BasicRollMechanic', function () {
  const ruleset: Ruleset = createTestRuleset()

  const rollFactory: vi.Mocked<RollFactory> = {
    create: vi.fn(),
  }

  const speakerProvider: vi.Mocked<SpeakerProvider> = {
    getSpeaker: vi.fn(),
  }
  const messageCreator: vi.Mocked<RollMessageCreator> = {
    create: vi.fn(),
  }

  const localizer: vi.Mocked<Localizer> = {
    localize: vi.fn(),
  }

  BasicRollMechanicRule(
    ruleset,
    rollFactory,
    speakerProvider,
    messageCreator,
    localizer
  )

  const roll: vi.Mocked<Rollable & Messagable> = {
    roll: vi.fn(),
    total: 0,
    dice: [],
    toMessage: vi.fn(),
    data: {},
  }
  rollFactory.create.mockReturnValue(roll)
  roll.roll.mockReturnValue(roll)

  test.each([5, 15])('should make a 1d20 roll', async function (rollResult) {
    const rollMod = 4
    const targetValue = 10
    const success = {
      5: true,
      15: false,
    }[rollResult]
    roll.total = rollResult
    roll.dice = [{}]

    roll.toMessage.mockImplementation(
      (data, _) => new Promise((resolve) => resolve({} as any))
    )

    const result = await ruleset.execute(RollAttribute, {
      attributeName: 'TestAttribute',
      mod: rollMod,
      targetValue,
    })
    expect(rollFactory.create).toHaveBeenCalledWith(`1dz + ${rollMod}`)
    expect(roll.roll).toBeCalled()
    expect(result.success).toEqual(success)
  })

  test('should be able to make a basic roll', async function () {
    const rollResult = 3
    const formula = '1d6 + 2'
    const mod = 1
    roll.total = rollResult

    const result = await ruleset.execute(BasicRoll, {
      mod,
      formula,
    })
    expect(rollFactory.create).toHaveBeenCalledWith(`1d6 + 2 + 1`)

    expect(roll.roll).toBeCalled()
    expect(result.roll.total).toEqual(rollResult)
  })

  test.each([5, 15])(
    'should be able to confirm a critical 1d20 combat success roll',
    async function (criticalConfirmRollResult) {
      const rollResult = 1
      const rollMod = 4
      const targetValue = 10
      const critical = {
        5: true,
        15: false,
      }[criticalConfirmRollResult]
      const dice = [
        {
          secondRoll: criticalConfirmRollResult,
        },
      ]
      roll.total = rollResult
      roll.dice = dice

      const result = await ruleset.execute(RollCombatAttribute, {
        character: undefined as unknown as BaseCharacter,
        weapon: undefined,
        attributeName: 'TestAttribute',
        mod: rollMod,
        targetValue,
      })
      expect(rollFactory.create).toHaveBeenCalledWith(`1dzcc + ${rollMod}`)

      expect(roll.roll).toBeCalled()
      expect(result.success).toEqual(true)
      expect(result.critical).toEqual(critical)
    }
  )

  type TestAttributeData = { name: AttributeName; value: number }
  const testAttributeData: [
    TestAttributeData,
    TestAttributeData,
    TestAttributeData
  ] = [
    {
      name: 'courage',
      value: 10,
    },
    {
      name: 'cleverness',
      value: 11,
    },
    {
      name: 'agility',
      value: 12,
    },
  ]

  test.each([-5, 5])('should make a 3d20 roll', async function (rollResult) {
    const mod = 4
    const skillValue = 3
    // const valueModDiff = skillValue - mod
    const rollMod = -1 //Math.min(valueModDiff, 0)
    const totalMod = 0 //Math.max(valueModDiff, 0)
    const success = {
      5: true,
      '-5': false,
    }[rollResult]
    roll.total = rollResult

    const result = await ruleset.execute(RollSkill, {
      skillName: 'TestSkill',
      skillType: 'talent',
      mod,
      skillValue,
      testAttributeData,
    })
    const expectedRollFormula = `min(min((10-1d20+${rollMod}),0) + min((11-1d20+${rollMod}),0) + min((12-1d20+${rollMod}),0) + ${totalMod},${skillValue})`
    expect(rollFactory.create).toHaveBeenCalledWith(expectedRollFormula)

    expect(roll.roll).toBeCalled()
    expect(result.success).toEqual(success)
  })

  test.each([-5, 5])(
    'should make a 3d20 roll with positive roll bonus',
    async function (rollResult) {
      const mod = 4
      const skillValue = 5
      // const valueModDiff = skillValue - mod
      const rollMod = 0 //Math.min(valueModDiff, 0)
      const totalMod = skillValue - mod //Math.max(valueModDiff, 0)
      const success = {
        5: true,
        '-5': false,
      }[rollResult]
      roll.total = rollResult

      const result = await ruleset.execute(RollSkill, {
        skillName: 'TestSkill',
        skillType: 'talent',
        mod,
        skillValue,
        testAttributeData,
      })
      const expectedRollFormula = `min(min((10-1d20+${rollMod}),0) + min((11-1d20+${rollMod}),0) + min((12-1d20+${rollMod}),0) + ${totalMod},${skillValue})`
      expect(rollFactory.create).toHaveBeenCalledWith(expectedRollFormula)

      expect(roll.roll).toBeCalled()
      expect(result.success).toEqual(success)
    }
  )

  test.each([-5, 5])(
    'should make a 3d20 roll with positive roll bonus',
    async function (rollResult) {
      const mod = -4
      const skillValue = 5
      // const valueModDiff = skillValue - mod
      const rollMod = 0 //Math.min(valueModDiff, 0)
      const totalMod = skillValue - mod //Math.max(valueModDiff, 0)
      const success = {
        5: true,
        '-5': false,
      }[rollResult]
      roll.total = rollResult

      const result = await ruleset.execute(RollSkill, {
        skillName: 'TestSkill',
        skillType: 'talent',
        mod,
        skillValue,
        testAttributeData,
      })
      const expectedRollFormula = `min(min((10-1d20+${rollMod}),0) + min((11-1d20+${rollMod}),0) + min((12-1d20+${rollMod}),0) + ${totalMod},${skillValue})`
      expect(rollFactory.create).toHaveBeenCalledWith(expectedRollFormula)

      expect(roll.roll).toBeCalled()
      expect(result.success).toEqual(success)
    }
  )
})

describe('Basic Roll To Chat', function () {
  const ruleset: Ruleset = createTestRuleset()

  const speakerProvider: vi.Mocked<SpeakerProvider> = {
    getSpeaker: vi.fn(),
  }
  const messageCreator: vi.Mocked<RollMessageCreator> = {
    create: vi.fn(),
  }

  const actionExecutor = vi.fn()
  let actionResult = {}
  actionExecutor.mockImplementation((_options) => actionResult)
  const actionIdentifier = CreateActionIdentifier('basic')
  ruleset.registerAction(new TestAction(actionIdentifier, actionExecutor))
  const localizer: vi.Mocked<Localizer> = {
    localize: vi.fn(),
  }
  ruleset.registerEffect<BasicRollActionResult, BasicRollChatEffect>(
    new BasicRollChatEffect(
      BasicRollToChatEffect,
      speakerProvider,
      messageCreator,
      localizer
    )
  )
  ruleset.after(actionIdentifier).trigger(BasicRollToChatEffect)

  const roll: vi.Mocked<Rollable & Messagable> = {
    roll: vi.fn(),
    total: 0,
    dice: [],
    toMessage: vi.fn(),
    data: {},
  }

  test.each([
    {
      rollResult: 5,
      expectedFlavor: '',
    },
    {
      rollResult: 15,
      expectedFlavor: '',
    },
  ])(
    'should generate a message for each roll',
    async function ({ rollResult, expectedFlavor }) {
      const messageData: MessageData<{ flavor: string }> = {
        flavor: expectedFlavor,
        user: '',
        type: 5,
        content: 0,
        sound: '',
      }

      roll.total = rollResult
      roll.toMessage.mockImplementation(
        (data, _) =>
          new Promise((resolve) => {
            if (data.flavor === expectedFlavor)
              resolve({
                ...messageData,
                roll,
              } as any)
            else resolve({} as any)
          })
      )

      const speaker = 'Speaker'
      const speakerData: ReturnType<typeof ChatMessage.getSpeaker> = {
        scene: null,
        actor: null,
        token: null,
        alias: speaker,
      }
      speakerProvider.getSpeaker.mockReturnValue(speakerData)

      const expectedChatData = {
        ...messageData,
        roll,
      }

      const actionOptions = {}

      actionResult = {
        roll,
      }

      await ruleset.execute(actionIdentifier, actionOptions)

      expect(messageCreator.create).toHaveBeenCalledWith(
        expect.objectContaining(expectedChatData)
      )
    }
  )

  afterEach(() => {
    vi.clearAllMocks()
  })
})

describe('Attribute Roll To Chat', function () {
  const ruleset: Ruleset = createTestRuleset()

  const speakerProvider: vi.Mocked<SpeakerProvider> = {
    getSpeaker: vi.fn(),
  }
  const messageCreator: vi.Mocked<RollMessageCreator> = {
    create: vi.fn(),
  }

  const actionExecutor = vi.fn()
  let actionResult = {}
  actionExecutor.mockImplementation((_options) => actionResult)
  const actionIdentifier = CreateActionIdentifier('attribute')
  ruleset.registerAction(new TestAction(actionIdentifier, actionExecutor))
  const localizer: vi.Mocked<Localizer> = {
    localize: vi.fn(),
  }
  ruleset.registerEffect<AttributeActionResult, AttributeRollChatEffect>(
    new AttributeRollChatEffect(
      RollAttributeToChatEffect,
      speakerProvider,
      messageCreator,
      localizer
    )
  )
  ruleset.after(actionIdentifier).trigger(RollAttributeToChatEffect)

  const roll: vi.Mocked<Rollable & Messagable> = {
    roll: vi.fn(),
    total: 0,
    dice: [],
    toMessage: vi.fn(),
    data: {},
  }

  test.each([
    {
      rollResult: 5,
      success: true,
      critical: false,
      expectedFlavor: 'Mut (10) Erfolg',
    },
    {
      rollResult: 15,
      success: false,
      critical: false,
      expectedFlavor: 'Mut (10) Misserfolg',
    },
    {
      rollResult: 20,
      success: false,
      critical: true,
      expectedFlavor: 'Mut (10) Kritischer Misserfolg',
    },
    {
      rollResult: 20,
      success: false,
      critical: false,
      expectedFlavor: 'Mut (10) Misserfolg',
    },
    {
      rollResult: 1,
      success: true,
      critical: true,
      expectedFlavor: 'Mut (10) Kritischer Erfolg',
    },
    {
      rollResult: 1,
      success: true,
      critical: false,
      expectedFlavor: 'Mut (10) Erfolg',
    },
  ])(
    'should generate a message for each roll',
    async function ({ rollResult, success, critical, expectedFlavor }) {
      const attributeName = 'courage'
      const targetValue = 10
      const localizedName = 'Mut'

      const successInfoI18n = success ? 'Erfolg' : 'Misserfolg'
      const successInfo = success ? 'success' : 'failed'
      const additionalFlavor = 'Wuchtschlag +3'
      const additionalContent = 'Additional Content'
      const damage = '1d6 + 3'
      const penality = 4
      const messageData: MessageData<{ flavor: string }> = {
        flavor: expectedFlavor,
        user: '',
        type: 5,
        content: 0,
        sound: '',
      }
      when(localizer.localize)
        .calledWith(`DSA.${attributeName}`)
        .mockReturnValue(localizedName)
      when(localizer.localize)
        .calledWith(`DSA.${successInfo}`)
        .mockReturnValue(successInfoI18n)

      when(localizer.localize)
        .calledWith(`DSA.critical`)
        .mockReturnValue('Kritischer')
      roll.total = rollResult
      roll.toMessage.mockImplementation(
        (data, _) =>
          new Promise((resolve) => {
            if (data.flavor === expectedFlavor)
              resolve({
                ...messageData,
                roll,
              } as any)
            else resolve({} as any)
          })
      )

      // const chatData = {
      //   ...messageData,
      //   roll: {
      //     data: {},
      //   },
      // }
      // when(roll.toMessage)
      //   .calledWith(expect.objectContaining(messageData), expect.anything())
      //   .mockReturnValue(chatData)

      const speaker = 'Speaker'
      const speakerData: ReturnType<typeof ChatMessage.getSpeaker> = {
        scene: null,
        actor: null,
        token: null,
        alias: speaker,
      }
      speakerProvider.getSpeaker.mockReturnValue(speakerData)

      const expectedChatData = {
        ...messageData,
        roll: {
          ...roll,
          data: {
            damage,
            penality,
          },
        },
      }

      const actionOptions = {
        attributeName,
        targetValue,
        additionalFlavor,
        additionalContent,
      } as BaseActionOptionData

      actionResult = {
        attributeName,
        roll,
        damage: {
          formula: damage,
        },
        penality,
        success,
        critical,
        options: actionOptions,
      }

      await ruleset.execute(actionIdentifier, actionOptions)

      expect(messageCreator.create).toHaveBeenCalledWith(
        expect.objectContaining(expectedChatData)
      )

      expect(localizer.localize).toHaveBeenCalledWith(`DSA.${attributeName}`)
      expect(localizer.localize).toHaveBeenCalledWith(`DSA.${successInfo}`)
    }
  )

  afterEach(() => {
    vi.clearAllMocks()
  })
})

describe('Skill Roll To Chat', function () {
  const ruleset: Ruleset = createTestRuleset()

  const speakerProvider: vi.Mocked<SpeakerProvider> = {
    getSpeaker: vi.fn(),
  }
  const messageCreator: vi.Mocked<RollMessageCreator> = {
    create: vi.fn(),
  }

  const actionExecutor = vi.fn()
  let actionResult = {}
  actionExecutor.mockImplementation((_options) => actionResult)
  const actionIdentifier = RollSkill //CreateActionIdentifier('skill')
  ruleset.registerAction(new TestAction(actionIdentifier, actionExecutor))
  const localizer: vi.Mocked<Localizer> = {
    localize: vi.fn(),
  }
  ruleset.registerEffect<SkillActionResult, SkillRollChatEffect>(
    new SkillRollChatEffect(
      RollSkillToChatEffect,
      speakerProvider,
      messageCreator,
      localizer
    )
  )
  ruleset.after(actionIdentifier).trigger(RollSkillToChatEffect)

  type TestAttributeData = { name: AttributeName; value: number }
  const testAttributeData: [
    TestAttributeData,
    TestAttributeData,
    TestAttributeData
  ] = [
    {
      name: 'courage',
      value: 10,
    },
    {
      name: 'cleverness',
      value: 11,
    },
    {
      name: 'agility',
      value: 12,
    },
  ]

  test.each([-5, 5])(
    'should generate a message for each roll for 3d20 rolls',
    async function (rollResult) {
      const skillName = 'Swimming'
      const skillType = 'talent'
      const skillValue = 10
      const damage = '1d6'
      const mod = 5
      const success = {
        5: true,
        '-5': false,
      }[rollResult]
      const successInfoI18n = success ? 'Erfolg' : 'Misserfolg'
      const successInfo = success ? 'success' : 'failed'
      const expectedFlavor = `${skillName} +${mod} (${skillValue}) ${successInfoI18n}`
      const messageData: MessageData<{ flavor: string }> = {
        flavor: expectedFlavor,
        user: '',
        type: 5,
        content: 0,
        sound: '',
      }
      const chatData = {
        ...messageData,
        roll: {
          data: {},
        },
      }
      localizer.localize.mockImplementation(() => successInfoI18n)

      const roll: Messagable = {
        data: {},
        toMessage: vi.fn().mockImplementation(
          () =>
            new Promise((resolve) =>
              resolve({
                ...messageData,
                roll,
              })
            )
        ),
      }
      const actionOptions: SkillRollActionData = {
        skillName,
        skillType,
        skillValue,
        testAttributeData,
        mod,
      }
      actionResult = {
        roll,
        damage,
        success,
        options: actionOptions,
        skillCheckData: testAttributeData,
        skillType,
      }

      const expectedChatData = {
        ...chatData,
        roll: {
          data: {
            is3d20Roll: true,
            skillCheckData: testAttributeData,
            skillType,
            damage,
          },
          toMessage: roll.toMessage,
        },
      }

      const speaker = 'Speaker'
      const speakerData: ReturnType<typeof ChatMessage.getSpeaker> = {
        scene: null,
        actor: null,
        token: null,
        alias: speaker,
      }
      speakerProvider.getSpeaker.mockImplementation(() => speakerData)

      await ruleset.execute(actionIdentifier, actionOptions)

      expect(messageCreator.create).toHaveBeenCalledWith(
        expect.objectContaining(expectedChatData)
      )

      expect(localizer.localize).toHaveBeenCalledWith(`DSA.${successInfo}`)
    }
  )

  afterEach(() => {
    vi.clearAllMocks()
  })
})
