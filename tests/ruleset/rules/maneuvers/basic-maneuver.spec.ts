import {
  BasicManeuverRule,
  ComputeManeuverList,
} from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'

import {
  AttackAction,
  CombatActionData,
  CombatActionResult,
  DodgeAction,
  ParryAction,
} from '../../../../src/module/ruleset/rules/basic-combat.js'
import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { TestAction } from '../../test-classes.js'
import { createTestRuleset } from '../helpers.js'
import type { ActionIdentifier } from '../../../../src/module/ruleset/rule-components.js'
import { RangedAttackAction } from '../../../../src/module/ruleset/rules/basic-ranged-combat.js'
import type {
  ManeuverType,
  ModifierDescriptor,
} from '../../../../src/module/model/modifier.js'
import { Maneuver } from '../../../../src/module/character/maneuver.js'

describe('Basic Maneuver', function () {
  const ruleset = createTestRuleset()

  const executionResult = {} as any

  const executeHook = vi.fn().mockReturnValue(executionResult)

  ruleset.registerAction(new TestAction(AttackAction, executeHook))
  ruleset.registerAction(new TestAction(ParryAction, executeHook))
  ruleset.registerAction(new TestAction(DodgeAction, executeHook))
  ruleset.registerAction(new TestAction(RangedAttackAction, executeHook))

  ruleset.add(BasicManeuverRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  test.each([AttackAction, ParryAction])(
    'should add penality of failed maneuver to action result',
    async function (
      action: ActionIdentifier<CombatActionData, CombatActionResult>
    ) {
      const mightyStrike = new Maneuver('TestManeuver', 'offensive')
      mightyStrike.mod = 5
      const options = {
        character,
        modifiers: [mightyStrike],
        talent: undefined,
        weapon: undefined,
        mod: 0,
      }
      executionResult.options = options
      executionResult.success = false

      const expectedPenality = 5

      const result = await ruleset.execute(action, options)

      expect(result.penality).toEqual(expectedPenality)
    }
  )

  test.each([AttackAction, ParryAction, DodgeAction, RangedAttackAction])(
    'should add penality of failed maneuver to action result',
    async function (
      action: ActionIdentifier<CombatActionData, CombatActionResult>
    ) {
      const modifiers: ModifierDescriptor[] = [
        {
          name: 'TestManeuver',
          mod: 3,
          modifierType: 'maneuver',
        },

        {
          name: 'Test Modifier',
          mod: 4,
          modifierType: 'other',
        },
      ]
      const options = {
        character,
        modifiers,
        talent: undefined,
        weapon: undefined,
        mod: 0,
      }
      executionResult.options = options
      executionResult.success = false

      const expectedTotalMod = 7

      await ruleset.execute(action, options)

      expect(executeHook).toHaveBeenCalledWith(
        expect.objectContaining({
          mod: expectedTotalMod,
        })
      )
    }
  )

  test.each(['offensive', 'defensive'])(
    'should be able to compute a list of available maneuvers',
    function (maneuverType: ManeuverType) {
      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })
      expect(result.maneuvers).toEqual([])
    }
  )

  afterEach(() => {
    vi.clearAllMocks()
  })
})
