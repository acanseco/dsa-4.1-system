import { when } from 'jest-when'

import {
  MightyStrike,
  MightyStrikeManeuver,
  MightyStrikeRule,
} from '../../../../src/module/ruleset/rules/maneuvers/mighty-strike.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { ComputeDamageFormula } from '../../../../src/module/ruleset/rules/derived-combat-attributes.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'

describe('Mighty Strike', function () {
  const ruleset = createTestRuleset()

  const computationResult: Record<string, unknown> = {}
  const executeHook = vi.fn().mockReturnValue(computationResult)
  ruleset.registerComputation(
    new Computation(ComputeDamageFormula, executeHook)
  )
  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      vi.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(MightyStrikeRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter
  beforeEach(() => {
    computationResult.bonusDamage = 0
  })

  it('should compute bonus damage on success if character possess mighty strike', async function () {
    const mightyStrike = MightyStrikeManeuver
    mightyStrike.mod = 5
    const options = {
      character,
      modifiers: [mightyStrike],
    }
    computationResult.options = options
    computationResult.bonusDamage = 0
    character.has = vi.fn()

    when(character.has).calledWith(MightyStrike).mockReturnValue(true)

    const result = await ruleset.compute(ComputeDamageFormula, options)

    expect(result.bonusDamage).toEqual(mightyStrike.mod)
  })

  it('should only give half bonus damage if character does not possess mighty strike', async function () {
    const mightyStrike = MightyStrikeManeuver
    mightyStrike.mod = 5
    const expectedBonusDamage = 3
    const options = {
      character,
      modifiers: [mightyStrike],
      weapon: undefined,
    }
    computationResult.options = options
    computationResult.bonusDamage = 0
    when(character.has).calledWith(MightyStrike).mockReturnValue(false)

    const result = await ruleset.compute(ComputeDamageFormula, options)

    expect(result.bonusDamage).toEqual(expectedBonusDamage)
  })

  it('should add mighty strike to the list of offensive maneuvers', function () {
    const result = ruleset.compute(ComputeManeuverList, {
      character,
      maneuverType: 'offensive',
    })

    expect(result.maneuvers.includes(MightyStrikeManeuver)).toEqual(true)
  })

  it('should not add mighty strike to the list of defensive maneuvers', function () {
    const result = ruleset.compute(ComputeManeuverList, {
      character,
      maneuverType: 'defensive',
    })

    expect(result.maneuvers.includes(MightyStrikeManeuver)).toEqual(false)
  })
})
