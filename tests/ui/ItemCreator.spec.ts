import { describe, expect, test, vi } from 'vitest'
import { fireEvent, render } from '@testing-library/svelte'
import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import ItemCreator from '../../src/ui/ItemCreator.svelte'
import { writable } from 'svelte/store'

describe('ItemCreator', () => {
  test('Creates items', async () => {
    const itemType = 'super-item'
    const doc = {
      createEmbeddedDocuments: vi.fn(),
    }
    const context = {
      doc: writable(doc),
    }
    const { getByRole } = render(
      html`
      <${Fragment} context=${context}>
        <${ItemCreator} itemType=${itemType}/>
      </$>
      `
    )
    const link = getByRole('link') as HTMLAnchorElement
    expect(link).toBeInTheDocument()
    expect(link).toHaveTextContent('add')
    expect(link.firstChild?.nodeName).toBe('I')
    expect(link.firstChild).toHaveClass('fas fa-plus')

    await fireEvent.click(link)
    expect(doc.createEmbeddedDocuments).toHaveBeenCalledWith(
      'Item',
      [
        {
          img: 'icons/svg/item-bag.svg',
          name: 'newItem',
          type: itemType,
        },
      ],
      {
        renderSheet: true,
      }
    )
  })
})
