import { render } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../src/ui/Dialog.svelte',
  async () => await import('../MockDialog.svelte')
)

import { getGame } from '../../../src/module/utils.js'
;(getGame as Mock<any>).mockImplementation(() => ({
  ruleset: createMockRuleset(),
}))

import Attribute from '../../../src/ui/actor-sheet/Attribute.svelte'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../ruleset/rules/helpers.js'
import { writable } from 'svelte/store'

describe('Attribute', () => {
  test('Is rendered correctly and can update the value', async () => {
    const attributeName = 'courage'
    const value = 5
    const context = {
      doc: writable({
        system: {
          base: {
            basicAttributes: {
              [attributeName]: {
                value,
              },
            },
          },
        },
      }),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Attribute} attributeName=${attributeName} />
      </$>
      `
    )

    expect(getByTestId('attribute-name')).toHaveTextContent(attributeName)
    expect(getByTestId('attribute-roll')).toHaveTextContent(value.toString())
  })
})
