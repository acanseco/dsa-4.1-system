import { render } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../src/ui/Dialog.svelte',
  async () => await import('../MockDialog.svelte')
)

import { getGame } from '../../../src/module/utils.js'
;(getGame as Mock<any>).mockImplementation(() => ({
  ruleset: createMockRuleset(),
}))

import Attributes from '../../../src/ui/actor-sheet/Attributes.svelte'
import { writable } from 'svelte/store'
import { createMockRuleset } from '../../ruleset/rules/helpers.js'
import type { Mock } from 'vitest'
import { attributeNames } from '../../../src/module/model/character-data.js'

describe('Attributes', () => {
  test('Is rendered correctly', async () => {
    const attribute = {
      value: 5,
    }
    const context = {
      doc: writable({
        system: {
          base: {
            basicAttributes: attributeNames.reduce(
              (a, v) => ({ ...a, [v]: attribute }),
              {}
            ),
          },
        },
      }),
    }
    const { getAllByTestId } = render(
      html`
        <${Fragment} context=${context}>
        <${Attributes} />
        </$>
        `
    )

    const attributes = getAllByTestId('attribute-name')
    expect(attributes.map((attr) => attr.textContent)).toEqual(
      expect.arrayContaining([...attributeNames])
    )
  })
})
