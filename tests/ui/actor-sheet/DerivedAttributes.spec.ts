import { render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import DerivedAttributes from '../../../src/ui/actor-sheet/DerivedAttributes.svelte'
import { writable } from 'svelte/store'

describe('DerivedAttributes', () => {
  test('Is rendered correctly', async () => {
    const activeCombatAttributes = [
      'baseAttack',
      'baseParry',
      'baseInitiative',
      'baseRangedAttack',
      'dodge',
    ]
    const passiveCombatAttributes = ['magicResistance']
    const movementAttributes = ['speed']
    const attribute = {
      value: 5,
    }
    const doc = {
      system: {
        base: {
          movement: {
            speed: {
              value: 5,
            },
          },
          combatAttributes: {
            active: activeCombatAttributes.reduce(
              (a, v) => ({ ...a, [v]: attribute }),
              {}
            ),
            passive: {
              magicResistance: {
                value: 5,
              },
            },
          },
        },
      },
    }
    const context = {
      doc: writable(doc),
    }
    const { getAllByTestId } = render(
      html`
        <${Fragment} context=${context}>
        <${DerivedAttributes} />
        </$>
        `
    )

    const derivedAttributes = getAllByTestId('derived-attribute')
    expect(
      derivedAttributes.map(
        (group) => within(group).getByTestId('label').textContent
      )
    ).toEqual(
      expect.arrayContaining([
        ...activeCombatAttributes,
        ...passiveCombatAttributes,
        ...movementAttributes,
      ])
    )
  })
})
