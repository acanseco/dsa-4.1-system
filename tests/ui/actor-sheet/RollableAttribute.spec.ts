import { fireEvent, render } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../src/ui/Dialog.svelte',
  async () => await import('../MockDialog.svelte')
)
import RollableAttribute from '../../../src/ui/actor-sheet/RollableAttribute.svelte'
import type {
  GeneralNamedAttribute,
  Rollable,
} from '../../../src/module/model/properties.js'
import { writable } from 'svelte/store'

describe('RollableAttribute', () => {
  test('Is rendered correctly with value and uses dialog for rolling', async () => {
    const attribute = {
      value: 4,
      name: 'testName',
      roll: vi.fn(),
    } as Rollable<GeneralNamedAttribute>
    const context = {
      isEditMode: writable(false),
      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
          <${RollableAttribute} attribute=${attribute} value=${
        attribute.value
      } isLocalized=${true} />
      </$>`
    )

    const rollButton = getByTestId('attribute-roll')
    const name = getByTestId('attribute-name')
    const value = getByTestId('attribute-value')

    expect(rollButton).toBeInTheDocument()
    expect(name).toBeInTheDocument()
    expect(name).toHaveTextContent(attribute.name)
    expect(value).toHaveTextContent(attribute.value.toString())
    await fireEvent.click(rollButton)
    expect(context.openDialogMock).toHaveBeenCalled()
    expect(attribute.roll).toHaveBeenCalled()
  })
})
