import { fireEvent, render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)

import ItemList from '../../../../src/ui/actor-sheet/tabs/ItemList.svelte'
import { writable } from 'svelte/store'

describe('ItemList', () => {
  test('Is rendered correctly', async () => {
    const items = [
      {
        name: 'firstItem',
        id: '1',
        system: {
          weight: '5',
          quantity: '6',
          isConsumable: true,
        },
        type: 'genericItem',
        sheet: { render: vi.fn() },
        delete: vi.fn(),
      },
      {
        name: 'secondItem',
        id: '2',
        system: {
          weight: '5',
          quantity: '6',
          isConsumable: true,
        },
        type: 'genericItem',
      },
      {
        name: 'thirdItem',
        id: '3',
        system: {
          weight: '5',
          quantity: '6',
          isConsumable: true,
        },
        type: 'genericItem',
      },
    ]
    const context = {
      isEditMode: writable(true),

      '#external': {
        application: {
          _onDragStart: vi.fn(),
        },
      },

      openDialogMock: vi.fn(),
    }
    const { findAllByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${ItemList} items=${items} />
      </$>
      `
    )

    const itemList = await findAllByTestId('item-entry')
    expect(itemList.length).toEqual(3)
    expect(
      itemList.map((item) => within(item).getByTestId('item-name').textContent)
    ).toEqual(['firstItem(x6)', 'secondItem(x6)', 'thirdItem(x6)'])

    const firstItem = itemList[0]
    const editItem = within(firstItem).getByTestId('item-edit')
    await fireEvent.click(editItem)
    expect(items[0].sheet?.render).toHaveBeenCalled()

    const deleteItem = within(firstItem).getByTestId('item-delete')
    await fireEvent.click(deleteItem)
    expect(items[0].sheet?.render).toHaveBeenCalled()
  })
})
