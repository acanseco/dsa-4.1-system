import { fireEvent, render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'

import Talent from '../../../../src/ui/actor-sheet/tabs/Talent.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('Talent', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const value = 5

    const talent = {
      system: {
        value,
        test: {
          firstAttribute: 'test',
          secondAttribute: 'teste',
          thirdAttribute: 'testen',
        },
        effectiveEncumbarance: {
          type: 'none',
        },
      },
      type: 'talent',
      name: 'testTalent',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Talent} talent=${talent} />
      </$>
      `
    )

    const talentRoll = getByTestId('talent-roll')
    const talentEdit = getByTestId('talent-edit')
    const talentValue = within(getByTestId('talent-value')).getByRole(
      'spinbutton'
    )

    expect(talentEdit).toBeInTheDocument()
    expect(talentValue).toHaveValue(value)

    expect(talentRoll).toHaveTextContent(talent.name)
    expect(talentRoll).toHaveTextContent('test_abbr / teste_abbr / testen_abbr')

    await fireEvent.click(talentEdit)
    expect(talent.sheet.render).toHaveBeenCalled()

    await fireEvent.change(talentValue)
    expect(talent.update).toHaveBeenCalled()

    await fireEvent.click(talentRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(ruleset.execute).toHaveBeenCalled()
  })

  test('Is rendered correctly in view mode', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const value = 5

    const talent = {
      system: {
        value,
        test: {
          firstAttribute: 'test',
          secondAttribute: 'teste',
          thirdAttribute: 'testen',
        },
        effectiveEncumbarance: {
          type: 'none',
        },
      },
      type: 'talent',
      name: 'testTalent',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      isEditMode: writable(false),
      doc: writable({
        system: {
          base: {},
        },
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Talent} talent=${talent} />
      </$>
      `
    )

    const talentRoll = getByTestId('talent-roll')
    const talentValue = getByTestId('talent-value')

    expect(talentValue).toHaveTextContent(value.toString())

    expect(talentRoll).toHaveTextContent(talent.name)
    expect(talentRoll).toHaveTextContent('test_abbr / teste_abbr / testen_abbr')

    await fireEvent.click(talentRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(ruleset.execute).toHaveBeenCalled()
  })

  test('Is rendered correctly for combat talent', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const value = 5

    const talent = {
      system: {
        value,

        effectiveEncumbarance: {
          type: 'none',
        },
        combat: {
          category: 'melee',
          attack: 6,
          parry: 7,
        },
      },
      type: 'combatTalent',
      name: 'testTalent',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Talent} talent=${talent} />
      </$>
      `
    )

    const talentRoll = getByTestId('talent-roll')
    const talentEdit = getByTestId('talent-edit')
    const talentValue = within(getByTestId('talent-value')).getByRole(
      'spinbutton'
    )

    expect(talentEdit).toBeInTheDocument()
    expect(talentValue).toHaveValue(value)

    expect(talentRoll).toHaveTextContent(talent.name)
    expect(talentRoll).not.toHaveTextContent(
      'test_abbr / teste_abbr / testen_abbr'
    )

    await fireEvent.click(talentEdit)
    expect(talent.sheet.render).toHaveBeenCalled()

    await fireEvent.change(talentValue)
    expect(talent.update).toHaveBeenCalled()

    const talentAttackValue = within(getByTestId('talent-attack')).getByRole(
      'spinbutton'
    )

    expect(talentAttackValue).toHaveValue(talent.system.combat.attack)
    const talentParryValue = within(getByTestId('talent-parry')).getByRole(
      'spinbutton'
    )
    expect(talentParryValue).toHaveValue(talent.system.combat.parry)
  })
})
